#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------------------------------------------------------------------------
#Script writen by J.Jahan (johannes.jahan@gmail.com), last update 2023/08/04
#version: 2.2.0
#---------------------------------------------------------------------------------------------------
# |BQS EoS Module|
# ----------------
# Definition of a function 'Output_dat_to_YAML(, input_coef, switchers)' which converts .dat files
# from the C++ core code into whatever format file required by user, using the Porter library.
#
# Arguments of the function:
#  - output_path: path to all the output files created by the core code to be converted ("EoS_BQS")
#  - switchers: dictionnary of booleans indicating which variables have to be stored in the  
#               output YAML files. 
#
#    Default structure (labels have to be the same than the corresponding variable labels):
#       {'P':, 's_dens':, 'E_dens':, 'B_dens':, 'Q_dens':, 'S_dens':, 'c_s':,
#        'P_derivatives_finite_mu':, 'Chis_zero_mu':, 'dChisdT_zero_mu':, 'd2ChisdT2_zero_mu':}
# 
# The produced YAML files will be stored in 'out_path', where the .dat files are stored.
# 
# The files then created are:
#   - 'BQS_eos_output_thermodynamics.yaml' containing the schema 'EoS_thermodynamics'
#     (if at least 1 of the corresponding switchers is 'True')
#   - 'BQS_eos_output_chis_derivatives.yaml' containing the schema 'EoS_chis_derivatives'
#     (if at least 1 of the corresponding switchers is 'True')
# 
# See API specs about the YAML structure for output files in: 
#   ./OpenAPI_Specifications-BQS_EoS.yaml
####################################################################################################

import sys as sys
import os as os
import yaml as yaml
import numpy as np

from muses_porter import Porter

def Output_dat_to_CSV(out_path, specifications, switchers):
    #-----------------
    # Input variables
    #-----------------
    print("")
    
    #--- Input files ---
    
    #Make a list of input files to read from (including path)
    input_files = ["EoS_Taylor_AllMu.dat"]
    if switchers["P_derivatives_finite_mu"]:
        input_files.append("EoS_Taylor_AllMu_Derivatives.dat")
    if switchers["Chis_zero_mu"]:
        input_files.append("All_Chis.dat")
    if switchers["dChisdT_zero_mu"]:
        input_files.append("All_DChisDT.dat")
    if switchers["d2ChisdT2_zero_mu"]:
        input_files.append("All_D2ChisDT2.dat")

    #--- Security checks ---
  
    #Ensuring path to files exists
    if not os.path.exists(out_path):
        with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
            yaml.dump({"code":400, "message":"[Output_dat_to_YAML]> Path " + out_path + " does not exist."}, outfile, default_flow_style=False)
        sys.exit("[Output_dat_to_YAML]> Path " + out_path + " does not exist\n\nOPERATION ABORTED")

    #Ensuring input files are existing
    for input_file in input_files:
        in_file = os.path.join(out_path, input_file)
        if not os.path.exists(in_file):
            with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
                yaml.dump({"code":400, "message":"[Output_dat_to_YAML]> File " + in_file + " not found."}, outfile, default_flow_style=False)
            sys.exit("[Output_dat_to_YAML]> File " + in_file + " not found\n\nOPERATION ABORTED")

    #--- Output files --- 
    file_thermo = "BQS_eos_output_thermodynamics.csv"
    meta_thermo = "BQS_eos_metadata_thermodynamics.yaml"
    
    file_chis = "BQS_eos_output_chis.csv"
    meta_chis = "BQS_eos_metadata_chis.yaml"
    
    #-------------------
    # Generic variables
    #-------------------
    
    #Initialise porter objects
    porter_thermo = Porter()
    porter_chis = Porter()
    
    #Dictionnaries to contain arrays of output variables
    data = {
        "T": [],
        "mu_B": [],
        "mu_Q": [],
        "mu_S": [],
        "P": [],
        "s_dens": [],
        "B_dens": [],
        "Q_dens": [],
        "S_dens": [],
        "E_dens": [],
        "c_s": [],
        "d2PdB2": [],
        "d2PdQ2": [],
        "d2PdS2": [],
        "d2PdBdQ": [],
        "d2PdBdS": [],
        "d2PdQdS": [],
        "d2PdTdB": [],
        "d2PdTdQ": [],
        "d2PdTdS": [],
        "d2PdT2": []
    }
    
    chis = {
        "T": [],
        "Chi000": [],
        "Chi200": [],
        "Chi020": [],
        "Chi002": [],
        "Chi110": [],
        "Chi101": [],
        "Chi011": [],
        "Chi400": [],
        "Chi040": [],
        "Chi004": [],
        "Chi310": [],
        "Chi301": [],
        "Chi031": [],
        "Chi130": [],
        "Chi103": [],
        "Chi013": [],
        "Chi220": [],
        "Chi202": [],
        "Chi022": [],
        "Chi211": [],
        "Chi121": [],
        "Chi112": [],
        "dChi000dT": [],
        "dChi200dT": [],
        "dChi020dT": [],
        "dChi002dT": [],
        "dChi110dT": [],
        "dChi101dT": [],
        "dChi011dT": [],
        "dChi400dT": [],
        "dChi040dT": [],
        "dChi004dT": [],
        "dChi310dT": [],
        "dChi301dT": [],
        "dChi031dT": [],
        "dChi130dT": [],
        "dChi103dT": [],
        "dChi013dT": [],
        "dChi220dT": [],
        "dChi202dT": [],
        "dChi022dT": [],
        "dChi211dT": [],
        "dChi121dT": [],
        "dChi112dT": [],
        "d2Chi000dT2": [],
        "d2Chi200dT2": [],
        "d2Chi020dT2": [],
        "d2Chi002dT2": [],
        "d2Chi110dT2": [],
        "d2Chi101dT2": [],
        "d2Chi011dT2": [],
        "d2Chi400dT2": [],
        "d2Chi040dT2": [],
        "d2Chi004dT2": [],
        "d2Chi310dT2": [],
        "d2Chi301dT2": [],
        "d2Chi031dT2": [],
        "d2Chi130dT2": [],
        "d2Chi103dT2": [],
        "d2Chi013dT2": [],
        "d2Chi220dT2": [],
        "d2Chi202dT2": [],
        "d2Chi022dT2": [],
        "d2Chi211dT2": [],
        "d2Chi121dT2": [],
        "d2Chi112dT2": [],
    }
    
    #--- List of variables to keep ---
    #Initialise with coordinates
    thermo_variables_to_keep = ["T", "mu_B","mu_Q","mu_S"]
    chis_to_keep = ["T"]
    
    #Complete with variables to keep according ot switchers
    for key in switchers.keys():
        if switchers[key]:
            if 'P_derivatives_finite_mu' in key:
                thermo_variables_to_keep.append('d2PdB2')
                thermo_variables_to_keep.append('d2PdQ2')
                thermo_variables_to_keep.append('d2PdS2')
                thermo_variables_to_keep.append('d2PdBdQ')
                thermo_variables_to_keep.append('d2PdBdS')
                thermo_variables_to_keep.append('d2PdQdS')
                thermo_variables_to_keep.append('d2PdTdB')
                thermo_variables_to_keep.append('d2PdTdQ')
                thermo_variables_to_keep.append('d2PdTdS')
                thermo_variables_to_keep.append('d2PdT2')
            elif "Chis_zero_mu" in key:
                chis_to_keep.append("Chi000")
                chis_to_keep.append("Chi200")
                chis_to_keep.append("Chi020")
                chis_to_keep.append("Chi002")
                chis_to_keep.append("Chi110")
                chis_to_keep.append("Chi101")
                chis_to_keep.append("Chi011")
                chis_to_keep.append("Chi400")
                chis_to_keep.append("Chi040")
                chis_to_keep.append("Chi004")
                chis_to_keep.append("Chi310")
                chis_to_keep.append("Chi301")
                chis_to_keep.append("Chi031")
                chis_to_keep.append("Chi130")
                chis_to_keep.append("Chi103")
                chis_to_keep.append("Chi013")
                chis_to_keep.append("Chi220")
                chis_to_keep.append("Chi202")
                chis_to_keep.append("Chi022")
                chis_to_keep.append("Chi211")
                chis_to_keep.append("Chi121")
                chis_to_keep.append("Chi112")
            elif "dChisdT_zero_mu" in key:
                chis_to_keep.append("dChi000dT")
                chis_to_keep.append("dChi200dT")
                chis_to_keep.append("dChi020dT")
                chis_to_keep.append("dChi002dT")
                chis_to_keep.append("dChi110dT")
                chis_to_keep.append("dChi101dT")
                chis_to_keep.append("dChi011dT")
                chis_to_keep.append("dChi400dT")
                chis_to_keep.append("dChi040dT")
                chis_to_keep.append("dChi004dT")
                chis_to_keep.append("dChi310dT")
                chis_to_keep.append("dChi301dT")
                chis_to_keep.append("dChi031dT")
                chis_to_keep.append("dChi130dT")
                chis_to_keep.append("dChi103dT")
                chis_to_keep.append("dChi013dT")
                chis_to_keep.append("dChi220dT")
                chis_to_keep.append("dChi202dT")
                chis_to_keep.append("dChi022dT")
                chis_to_keep.append("dChi211dT")
                chis_to_keep.append("dChi121dT")
                chis_to_keep.append("dChi112dT")
            elif "d2ChisdT2_zero_mu" in key:
                chis_to_keep.append("d2Chi000dT2")
                chis_to_keep.append("d2Chi200dT2")
                chis_to_keep.append("d2Chi020dT2")
                chis_to_keep.append("d2Chi002dT2")
                chis_to_keep.append("d2Chi110dT2")
                chis_to_keep.append("d2Chi101dT2")
                chis_to_keep.append("d2Chi011dT2")
                chis_to_keep.append("d2Chi400dT2")
                chis_to_keep.append("d2Chi040dT2")
                chis_to_keep.append("d2Chi004dT2")
                chis_to_keep.append("d2Chi310dT2")
                chis_to_keep.append("d2Chi301dT2")
                chis_to_keep.append("d2Chi031dT2")
                chis_to_keep.append("d2Chi130dT2")
                chis_to_keep.append("d2Chi103dT2")
                chis_to_keep.append("d2Chi013dT2")
                chis_to_keep.append("d2Chi220dT2")
                chis_to_keep.append("d2Chi202dT2")
                chis_to_keep.append("d2Chi022dT2")
                chis_to_keep.append("d2Chi211dT2")
                chis_to_keep.append("d2Chi121dT2")
                chis_to_keep.append("d2Chi112dT2")
            else:
                thermo_variables_to_keep.append(key)
    
    #---------------------------
    # Reading .dat output files
    #---------------------------
    
    # Thermodynamics
    #----------------
    if switchers['P'] or switchers['s_dens'] or switchers['B_dens'] or switchers['Q_dens'] or switchers['S_dens'] or switchers['E_dens'] or switchers['c_s']:
        #Extracting data from text file
        in_file = np.loadtxt(os.path.join(out_path, input_files[0]))
    
        #Loop over lines <=> data points
        for point in in_file:
            #Dictionnary for EoS points
            EoS_pnt = {}
            #Assigning values to corresponding variables
            data['T'].append(float(point[0]))
            data['mu_B'].append(float(point[1]))
            data['mu_Q'].append(float(point[2]))
            data['mu_S'].append(float(point[3]))
            if switchers['P']: data['P'].append(float(point[4]))
            else: data['P'].append(0)
            if switchers['s_dens']: data['s_dens'].append(float(point[5]))
            else: data['s_dens'].append(0)
            if switchers['B_dens']: data['B_dens'].append(float(point[6]))
            else: data['B_dens'].append(0)
            if switchers['Q_dens']: data['Q_dens'].append(float(point[7]))
            else: data['Q_dens'].append(0)
            if switchers['S_dens']: data['S_dens'].append(float(point[8]))
            else: data['S_dens'].append(0)
            if switchers['E_dens']: data['E_dens'].append(float(point[9]))
            else: data['E_dens'].append(0)
            if switchers['c_s']: data['c_s'].append(float(point[10]))
            else: data['c_s'].append(0)
            #Assign all derivatives to 0 if not required
            if not switchers["P_derivatives_finite_mu"]:
                data['d2PdB2'].append(0)
                data['d2PdQ2'].append(0)
                data['d2PdS2'].append(0)
                data['d2PdBdQ'].append(0)
                data['d2PdBdS'].append(0)
                data['d2PdQdS'].append(0)
                data['d2PdTdB'].append(0)
                data['d2PdTdQ'].append(0)
                data['d2PdTdS'].append(0)
                data['d2PdT2'].append(0)
            
        # Pressure derivatives
        #----------------------
        if switchers["P_derivatives_finite_mu"]:
            #Extracting data from text file
            in_file = np.loadtxt(os.path.join(out_path, "EoS_Taylor_AllMu_Derivatives.dat"))
    
            #Loop over lines <=> data points
            for point in in_file:
                data['d2PdB2'].append(float(point[4]))
                data['d2PdQ2'].append(float(point[5]))
                data['d2PdS2'].append(float(point[6]))
                data['d2PdBdQ'].append(float(point[7]))
                data['d2PdBdS'].append(float(point[8]))
                data['d2PdQdS'].append(float(point[9]))
                data['d2PdTdB'].append(float(point[10]))
                data['d2PdTdQ'].append(float(point[11]))
                data['d2PdTdS'].append(float(point[12]))
                data['d2PdT2'].append(float(point[13]))
        
        #Adding data to porter object 
        porter_thermo.set_data(
            #Adding dataset
            data,
            #Adding metadata
            {
                "T": ("MeV", float),
                "mu_B": ("MeV", float),
                "mu_S": ("MeV", float),
                "mu_Q": ("MeV", float),
                "P": ("MeV^4", float),
                "s_dens": ("MeV^3", float),
                "B_dens": ("MeV^3", float),
                "Q_dens": ("MeV^3", float),
                "S_dens": ("MeV^3", float),
                "E_dens": ("MeV^4", float),
                "c_s": ("None", float),
                "d2PdB2": ("MeV^2", float),
                "d2PdQ2": ("MeV^2", float),
                "d2PdS2": ("MeV^2", float),
                "d2PdBdQ": ("MeV^2", float),
                "d2PdBdS": ("MeV^2", float),
                "d2PdQdS": ("MeV^2", float),
                "d2PdTdB": ("MeV^2", float),
                "d2PdTdQ": ("MeV^2", float),
                "d2PdTdS": ("MeV^2", float),
                "d2PdT2": ("MeV^2", float)
            },
            #Do not record variables with empty values
            dropna=True,
        )
    
    # Chis and derivatives
    #----------------------
    if switchers["Chis_zero_mu"] or switchers["dChisdT_zero_mu"] or switchers["d2ChisdT2_zero_mu"]:
        #To indicate if T values have been listed yet or no
        T_list = False
        
        # Chis
        #------
        if switchers["Chis_zero_mu"]:
            #Extracting data from text file
            in_file = np.loadtxt(os.path.join(out_path, "All_Chis.dat"))
            #Loop over lines <=> data points
            for point in in_file:
                #Adding T
                chis['T'].append(float(point[0]))
                #Adding chis
                chis['Chi000'].append(float(point[1]))
                chis['Chi200'].append(float(point[2]))
                chis['Chi020'].append(float(point[3]))
                chis['Chi002'].append(float(point[4]))
                chis['Chi110'].append(float(point[5]))
                chis['Chi101'].append(float(point[6]))
                chis['Chi011'].append(float(point[7]))
                chis['Chi400'].append(float(point[8]))
                chis['Chi040'].append(float(point[9]))
                chis['Chi004'].append(float(point[10]))
                chis['Chi310'].append(float(point[11]))
                chis['Chi301'].append(float(point[12]))
                chis['Chi031'].append(float(point[13]))
                chis['Chi130'].append(float(point[14]))
                chis['Chi103'].append(float(point[15]))
                chis['Chi013'].append(float(point[16]))
                chis['Chi220'].append(float(point[17]))
                chis['Chi202'].append(float(point[18]))
                chis['Chi022'].append(float(point[19]))
                chis['Chi211'].append(float(point[20]))
                chis['Chi121'].append(float(point[21]))
                chis['Chi112'].append(float(point[22]))
            T_list = True
            
            
        # dChis/dT
        #----------
        if switchers["dChisdT_zero_mu"]:
            #Extracting data from text file
            in_file = np.loadtxt(os.path.join(out_path, "All_DChisDT.dat"))
    
            #Loop over lines <=> data points
            for point in in_file:
                #Adding T (if not yet added) 
                if not T_list:
                    chis['T'].append(float(point[0]))
                #Adding dchis/dT
                chis['dChi000dT'].append(float(point[1]))
                chis['dChi200dT'].append(float(point[2]))
                chis['dChi020dT'].append(float(point[3]))
                chis['dChi002dT'].append(float(point[4]))
                chis['dChi110dT'].append(float(point[5]))
                chis['dChi101dT'].append(float(point[6]))
                chis['dChi011dT'].append(float(point[7]))
                chis['dChi400dT'].append(float(point[8]))
                chis['dChi040dT'].append(float(point[9]))
                chis['dChi004dT'].append(float(point[10]))
                chis['dChi310dT'].append(float(point[11]))
                chis['dChi301dT'].append(float(point[12]))
                chis['dChi031dT'].append(float(point[13]))
                chis['dChi130dT'].append(float(point[14]))
                chis['dChi103dT'].append(float(point[15]))
                chis['dChi013dT'].append(float(point[16]))
                chis['dChi220dT'].append(float(point[17]))
                chis['dChi202dT'].append(float(point[18]))
                chis['dChi022dT'].append(float(point[19]))
                chis['dChi211dT'].append(float(point[20]))
                chis['dChi121dT'].append(float(point[21]))
                chis['dChi112dT'].append(float(point[22]))
            T_list = True
            
        # d2Chis/dT2
        #------------
        if switchers["d2ChisdT2_zero_mu"]:
            #Extracting data from text file
            in_file = np.loadtxt(os.path.join(out_path, "All_D2ChisDT2.dat"))
    
            #Loop over lines <=> data points
            for point in in_file:
                #Adding T (if not yet added) 
                if not T_list:
                    chis['T'].append(float(point[0]))
                #Adding dchis/dT
                chis['d2Chi000dT2'].append(float(point[1]))
                chis['d2Chi200dT2'].append(float(point[2]))
                chis['d2Chi020dT2'].append(float(point[3]))
                chis['d2Chi002dT2'].append(float(point[4]))
                chis['d2Chi110dT2'].append(float(point[5]))
                chis['d2Chi101dT2'].append(float(point[6]))
                chis['d2Chi011dT2'].append(float(point[7]))
                chis['d2Chi400dT2'].append(float(point[8]))
                chis['d2Chi040dT2'].append(float(point[9]))
                chis['d2Chi004dT2'].append(float(point[10]))
                chis['d2Chi310dT2'].append(float(point[11]))
                chis['d2Chi301dT2'].append(float(point[12]))
                chis['d2Chi031dT2'].append(float(point[13]))
                chis['d2Chi130dT2'].append(float(point[14]))
                chis['d2Chi103dT2'].append(float(point[15]))
                chis['d2Chi013dT2'].append(float(point[16]))
                chis['d2Chi220dT2'].append(float(point[17]))
                chis['d2Chi202dT2'].append(float(point[18]))
                chis['d2Chi022dT2'].append(float(point[19]))
                chis['d2Chi211dT2'].append(float(point[20]))
                chis['d2Chi121dT2'].append(float(point[21]))
                chis['d2Chi112dT2'].append(float(point[22]))
            T_list = True                    
        
        # Adding zero in case quantities are not recorded
        if not switchers["Chis_zero_mu"] or not switchers["dChisdT_zero_mu"] or not switchers["d2ChisdT2_zero_mu"]:
            #Looping over number of points
            for Temp in chis['T']:
                #Adding 0's if chis recorded
                if not switchers["Chis_zero_mu"]:
                    chis['Chi000'].append(0)
                    chis['Chi200'].append(0)
                    chis['Chi020'].append(0)
                    chis['Chi002'].append(0)
                    chis['Chi110'].append(0)
                    chis['Chi101'].append(0)
                    chis['Chi011'].append(0)
                    chis['Chi400'].append(0)
                    chis['Chi040'].append(0)
                    chis['Chi004'].append(0)
                    chis['Chi310'].append(0)
                    chis['Chi301'].append(0)
                    chis['Chi031'].append(0)
                    chis['Chi130'].append(0)
                    chis['Chi103'].append(0)
                    chis['Chi013'].append(0)
                    chis['Chi220'].append(0)
                    chis['Chi202'].append(0)
                    chis['Chi022'].append(0)
                    chis['Chi211'].append(0)
                    chis['Chi121'].append(0)
                    chis['Chi112'].append(0)
            
                #Adding 0's if not recorded
                if not switchers["dChisdT_zero_mu"]:
                    chis['dChi000dT'].append(0)
                    chis['dChi200dT'].append(0)
                    chis['dChi020dT'].append(0)
                    chis['dChi002dT'].append(0)
                    chis['dChi110dT'].append(0)
                    chis['dChi101dT'].append(0)
                    chis['dChi011dT'].append(0)
                    chis['dChi400dT'].append(0)
                    chis['dChi040dT'].append(0)
                    chis['dChi004dT'].append(0)
                    chis['dChi310dT'].append(0)
                    chis['dChi301dT'].append(0)
                    chis['dChi031dT'].append(0)
                    chis['dChi130dT'].append(0)
                    chis['dChi103dT'].append(0)
                    chis['dChi013dT'].append(0)
                    chis['dChi220dT'].append(0)
                    chis['dChi202dT'].append(0)
                    chis['dChi022dT'].append(0)
                    chis['dChi211dT'].append(0)
                    chis['dChi121dT'].append(0)
                    chis['dChi112dT'].append(0)
        
                #Adding 0's if not recorded
                if not switchers["d2ChisdT2_zero_mu"]:
                    chis['d2Chi000dT2'].append(0)
                    chis['d2Chi200dT2'].append(0)
                    chis['d2Chi020dT2'].append(0)
                    chis['d2Chi002dT2'].append(0)
                    chis['d2Chi110dT2'].append(0)
                    chis['d2Chi101dT2'].append(0)
                    chis['d2Chi011dT2'].append(0)
                    chis['d2Chi400dT2'].append(0)
                    chis['d2Chi040dT2'].append(0)
                    chis['d2Chi004dT2'].append(0)
                    chis['d2Chi310dT2'].append(0)
                    chis['d2Chi301dT2'].append(0)
                    chis['d2Chi031dT2'].append(0)
                    chis['d2Chi130dT2'].append(0)
                    chis['d2Chi103dT2'].append(0)
                    chis['d2Chi013dT2'].append(0)
                    chis['d2Chi220dT2'].append(0)
                    chis['d2Chi202dT2'].append(0)
                    chis['d2Chi022dT2'].append(0)
                    chis['d2Chi211dT2'].append(0)
                    chis['d2Chi121dT2'].append(0)
                    chis['d2Chi112dT2'].append(0)

        #Adding data to porter object 
        porter_chis.set_data(
            #Adding dataset
            chis,
            #Adding metadata
            {
                "T": ("MeV", float),
                "Chi000": ("None", float),
                "Chi200": ("None", float),
                "Chi020": ("None", float),
                "Chi002": ("None", float),
                "Chi110": ("None", float),
                "Chi101": ("None", float),
                "Chi011": ("None", float),
                "Chi400": ("None", float),
                "Chi040": ("None", float),
                "Chi004": ("None", float),
                "Chi310": ("None", float),
                "Chi301": ("None", float),
                "Chi031": ("None", float),
                "Chi130": ("None", float),
                "Chi103": ("None", float),
                "Chi013": ("None", float),
                "Chi220": ("None", float),
                "Chi202": ("None", float),
                "Chi022": ("None", float),
                "Chi211": ("None", float),
                "Chi121": ("None", float),
                "Chi112": ("None", float),
                "dChi000dT": ("None", float),
                "dChi200dT": ("None", float),
                "dChi020dT": ("None", float),
                "dChi002dT": ("None", float),
                "dChi110dT": ("None", float),
                "dChi101dT": ("None", float),
                "dChi011dT": ("None", float),
                "dChi400dT": ("None", float),
                "dChi040dT": ("None", float),
                "dChi004dT": ("None", float),
                "dChi310dT": ("None", float),
                "dChi301dT": ("None", float),
                "dChi031dT": ("None", float),
                "dChi130dT": ("None", float),
                "dChi103dT": ("None", float),
                "dChi013dT": ("None", float),
                "dChi220dT": ("None", float),
                "dChi202dT": ("None", float),
                "dChi022dT": ("None", float),
                "dChi211dT": ("None", float),
                "dChi121dT": ("None", float),
                "dChi112dT": ("None", float),
                "d2Chi000dT2": ("None", float),
                "d2Chi200dT2": ("None", float),
                "d2Chi020dT2": ("None", float),
                "d2Chi002dT2": ("None", float),
                "d2Chi110dT2": ("None", float),
                "d2Chi101dT2": ("None", float),
                "d2Chi011dT2": ("None", float),
                "d2Chi400dT2": ("None", float),
                "d2Chi040dT2": ("None", float),
                "d2Chi004dT2": ("None", float),
                "d2Chi310dT2": ("None", float),
                "d2Chi301dT2": ("None", float),
                "d2Chi031dT2": ("None", float),
                "d2Chi130dT2": ("None", float),
                "d2Chi103dT2": ("None", float),
                "d2Chi013dT2": ("None", float),
                "d2Chi220dT2": ("None", float),
                "d2Chi202dT2": ("None", float),
                "d2Chi022dT2": ("None", float),
                "d2Chi211dT2": ("None", float),
                "d2Chi121dT2": ("None", float),
                "d2Chi112dT2": ("None", float)
            },
            #Do not record variables with empty values
            dropna=True,
        )    
    
    #----------------------
    # Printing output file
    #----------------------
    
    # Thermodynamics
    #----------------
    if switchers['P'] or switchers['s_dens'] or switchers['B_dens'] or switchers['Q_dens'] or switchers['S_dens'] or switchers['E_dens'] or switchers['c_s']:
        porter_thermo.export_table(
            os.path.join(out_path, file_thermo),
            extension = "CSV",
            filename_schema = specifications,
            schema = "EoS_thermodynamics",
            filename_metadata = os.path.join(out_path, meta_thermo),
            dropna = True,
            delimiter=",",
            variables_to_keep = thermo_variables_to_keep,
            float_format="%.12e"
        )
    
    # Chis and derivatives
    #----------------------
    if switchers["Chis_zero_mu"] or switchers["dChisdT_zero_mu"] or switchers["d2ChisdT2_zero_mu"]:
        porter_chis.export_table(
            os.path.join(out_path, file_chis),
            extension = "CSV",
            filename_schema = specifications,
            schema = "EoS_chis_derivatives",
            filename_metadata = os.path.join(out_path, meta_chis),
            dropna = True,
            delimiter=",",
            variables_to_keep = chis_to_keep,
            float_format="%.12e"
        )
    
    #------------------------------
    # Successful operation message
    #------------------------------
    print("[Output_adapter]> Successfully extracted data and stored them into the following file(s):")
    if switchers['P'] or switchers['s_dens'] or switchers['B_dens'] or switchers['Q_dens'] or switchers['S_dens'] or switchers['E_dens'] or switchers['c_s']:
        print("\t- "+file_thermo+"   (metadata in: "+meta_thermo+")")
    if switchers["Chis_zero_mu"] or switchers["dChisdT_zero_mu"] or switchers["d2ChisdT2_zero_mu"]:
        print("\t- "+file_chis+"   (metadata in: "+meta_chis+")")
    print(" ")
    
    return 0
    
