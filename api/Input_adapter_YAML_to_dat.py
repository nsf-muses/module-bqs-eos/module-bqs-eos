#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------------------------------------------------------------------------
#Script writen by J.Jahan (johannes.jahan@gmail.com), last update 2024/04/02
#version: 1.0.0
#---------------------------------------------------------------------------------------------------
# |BQS EoS Module|
# ----------------
# Definition of a function 'Input_YAML_to_dat(in_path, input_user, input_coef, switchers)' which 
# converts YAML files into .dat input parameter files for the C++ core code.
# 
# Arguments of the function:
#  - in_path: path to the input YAML files
#  - input_user: YAML input file containing the schema 'Input_BQS_EoS_user' 
#  - input_coef: YAML input file containing the schema 'Taylor_coefficients_parameters'
#  - specifications: OpenAPI specifications for the module
#  - switchers: dictionnary that will be filled with booleans indicating which variables have to be   
#               stored in the output YAML files. 
#    Default structure:
#       {'P':, 's_dens':, 'E_dens':, 'B_dens':, 'Q_dens':, 'S_dens':, 'c_s':,
#        'P_derivatives_finite_mu':, 'Chis_zero_mu':, 'dChisdT_zero_mu':, 'd2ChisdT2_zero_mu':}
# 
# The produced .dat files will be stored in "in_path" along with input YAML files.
# 
# The files then created are:
#   - 'Ranges_Parameters.dat' containing the desired ranges of T, muB, mQ, muS and associated steps
#   - 'Coefficients_Parameters.dat' containing the coefficients used to parametrise susceptiblities
# 
# See API specs about the YAML structure for input files in: 
#   ./OpenAPI_Specifications-BQS_EoS.yaml
####################################################################################################

import sys as sys
import os as os
import yaml as yaml


def Input_YAML_to_dat(in_path, input_user, input_coef, specifications, switchers={}):
    #-----------------
    # Input variables
    #-----------------
    print("")
    
    #--- Input files ---
    
    # Name of file containing user inputs for calculations
    ranges_yaml = os.path.join(in_path, input_user)
    
    # Name of file containing coefficients to parametrise susceptibilities
    params_yaml = os.path.join(in_path, input_coef)
    
    #--- Security checks ---
    
    #Ensuring path to files exists
    if not os.path.exists(in_path):
        with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
            yaml.dump({"code":400, "message":"[Input_dat_to_YAML]> Path " + in_path + " does not exist."}, outfile, default_flow_style=False)
        sys.exit("[Input_dat_to_YAML]> Path " + in_path + " does not exist\n\nOPERATION ABORTED")
    
    #Ensuring the files are existing
    for input_file in [ranges_yaml, params_yaml, specifications]:
        if not os.path.exists(input_file):
            with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
                yaml.dump({"code":400, "message":"[Input_YAML_to_dat]> File '" + input_file + "' not found."}, outfile, default_flow_style=False)
            sys.exit("[Input_YAML_to_dat]> File '" + input_file + "' not found\n\nOPERATION ABORTED")
  
    #--- Output files --- 
  
    # File containing user inputs for calculations
    ranges_dat = "Ranges_Parameters.dat"
    
    # File containing coefficients to parametrise susceptibilities
    params_dat = "Coefficients_Parameters.dat"
  
    #-------------------
    # Generic variables
    #-------------------
    #Dictionnary which will contain data from input file(s)
    Input_BQS = {}
    #Dictionnary which will contain coefficients for susceptilibities parametrisation
    Coeff_BQS = {}
    
    #Table of all lines of parameters that will be printed in the .dat file
    Input_dat = []
    #Table of all lines of coefficients that will be printed in the .dat file
    Coeff_dat = []

    #---------------------
    # Reading input files
    #---------------------
  
    #Reading inputs from the user
    with open(ranges_yaml) as infile:
        Input_BQS = yaml.safe_load(infile)
        
    #Reading coefficients for susceptibilities parametrisation
    with open(params_yaml) as infile:
        Coeff_BQS = yaml.safe_load(infile)
    
    #-----------------
    # Extracting data 
    #-----------------
  
    # Read user instructions and format for .dat input file
    #-------------------------------------------------------
    line = '' #temporary variable to store data before writing in output files
    
    range_parameters = ["T", "mu_B", "mu_Q", "mu_S"]
    
    #Looping over range parameters
    for parameter in range_parameters:
        line = ''
        #Recording the defined value
        line += str(Input_BQS["parameters"][parameter+"_min"]) + '\t'
        line += str(Input_BQS["parameters"][parameter+"_max"]) + '\t'
        line += str(Input_BQS["parameters"]["d"+parameter])
        #Record the line and reset the variable for the next line
        Input_dat.append(line)
    
    #Looping over switchers
    for keyprop in Input_BQS["switchers"]:
        #Record the switcher value
        switchers[keyprop[4:]] = Input_BQS["switchers"][keyprop]
                
    # Read coefficients for susceptibilities parametrisation and format for .dat input file
    #---------------------------------------------------------------------------------------
    line = '' #temporary variable to store data before writing in output files
    #To ensure coefs are printed in the correct order read by the code
    Chis_list = ["Chi000","Chi200","Chi020","Chi002","Chi110","Chi101","Chi011","Chi400","Chi040","Chi004","Chi310","Chi301","Chi031","Chi130","Chi103","Chi013","Chi220","Chi202","Chi022","Chi211","Chi121","Chi112"]
    Chis_coefs = ["a0","a1","a2","a3","a4","a5","a6","a7","a8","a9","b0","b1","b2","b3","b4","b5","b6","b7","b8","b9","c0"]
    Chi2B_coefs = ["h1","h2","f3","f4","f5"]
  
    #Looping over all susceptibilities
    for chi in Chis_list:
        chi = "coefs_"+chi
        #Particular case of Chi200: only 5 coefficients defined since different parametrisation
        if chi == "coefs_Chi200":
            #Looping over all coefficients
            for coef in Chi2B_coefs:
                line += str(Coeff_BQS[chi][coef]) + '\t'
            #Add 0's for all other unnecessary coefficients
            line += '0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0'
        else:
            #Looping over all coefficients for each susceptibility
            for coef in Chis_coefs:
                line += str(Coeff_BQS[chi][coef]) + '\t'
        #Record line of parameters if not empy
        if line != '':
            Coeff_dat.append(line)
            line = '' #reinitialise the variable for next line
    
    #--------------------
    # Printing .dat file
    #--------------------
    #File with user's inputs
    with open(os.path.join(in_path, ranges_dat), 'w') as outfile:
        for line in Input_dat:
            outfile.write(line + '\n')
    
    #File with susceptibility coefficients
    with open(os.path.join(in_path, params_dat), 'w') as outfile:
        for line in Coeff_dat:
            outfile.write(line + '\n')
  
    #------------------------------
    # Successful operation message
    #------------------------------
    print("[Input_YAML_to_dat]> Successfully converted the following files: \n\t"
        + input_user + "\t --> \t" + ranges_dat + "\n\t"
        + input_coef + "\t --> \t" + params_dat + "\n"
    )
    
    return 0
    
