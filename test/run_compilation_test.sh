#!/bin/bash

#####
# Test script to demonstrate:
#  - proper compilation of the code
#
# Written by J. Jahan
#####

## Set the errexit shell option to abort upon any error 
set -e

## Change to main directory of the repo
cd "$(dirname "$(readlink -f "$0")")"/../

##---------------##
## START MESSAGE ##
##---------------##
echo ""
echo "!-------------------------------------!"
echo "!  BQS EoS Module - Compilation test  !"
echo "!-------------------------------------!"
echo "Starting..."

## Initialize directories needed for Docker volumes
main_dir="$(pwd)/"

## Running compilation 
##---------------------
echo ""
echo "#--- Running compilation of the code ---#"
make redo

## Checking output files
##-----------------------
echo ""
echo "#--- Checking executable ---#"

## Defining expected executable
exec_file="EoS_BQS"

## Checking if executable is where it is supposed to be
if [ -f ${main_dir}${exec_file} ]; then
  echo "Found executable ${main_dir}${exec_file}"
  echo ""
else
  echo " /!\\ Executable NOT FOUND: ${exec_file}"
  echo ""
  echo "     ###################################"
  echo "     # /!\\ Compilation test FAILED /!\\ #"
  echo "     ###################################"
  echo ""
  exit 1 #Exit script with an error code
fi

##-------------##
## END MESSAGE ##
##-------------##
echo "      #################################"
echo "      #> Compilation test SUCCESSFUL <#"
echo "      #################################"
echo ""

exit 0 #Exit the script with a succes code
