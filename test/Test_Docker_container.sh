#!/bin/bash

#####
# Test script to demonstrate:
#  - Docker image building
#  - proper functioning of Docker image
#
# Written by J. Jahan, inspired by examples from A.T. Manning and Z. Zhang
#####

## Set the errexit shell option to abort upon any error 
set -e

## Change to main directory of the repository
cd "$(dirname "$(readlink -f "$0")")"/../

##-----------------------##
## BUILDING DOCKER IMAGE ##
##-----------------------##
bash ./test/run_Docker_build.sh

##------------------------------------------##
## TESTING CODE RUNNING INSIDE DOCKER IMAGE ##
##------------------------------------------##
echo ""
docker run -it --rm \
    eos_bqs-image:test \
    bash test/run_execution_test.sh

exit 0 #Exit the script with a succes code
