#!/bin/bash

#####
# Test script to demonstrate:
#  - proper execution of the code
#
# Written by J. Jahan
#####

## Set the errexit shell option to abort upon any error 
set -e

## Change to main directory of the repo
cd "$(dirname "$(readlink -f "$0")")"/../

##---------------##
## START MESSAGE ##
##---------------##
echo ""
echo "!-----------------------------------!"
echo "!  BQS EoS Module - Execution test  !"
echo "!-----------------------------------!"
echo "Starting..."

echo "Current directory: $(pwd)"

## Initializing directories needed for Docker volumes
main_dir="$(pwd)"
src_dir="${main_dir}/src/"
input_dir="${main_dir}/input/"
output_dir="${main_dir}/output/"
test_dir="${main_dir}/test/"

## Initializing input files
in_files=("config.yaml")

## Initializing expected output files
out_files=("BQS_eos_output_thermodynamics.csv" "BQS_eos_metadata_thermodynamics.yaml" "BQS_eos_output_chis.csv" "BQS_eos_metadata_chis.yaml")

## Executing the program using the test input files 
##--------------------------------------------------
echo ""
echo "#--- Running the code using test input files ---#"

## Copying test input files in 'input/' repository
for in_file in ${in_files[@]} ; do
  ## Temporarily renaming input file
  mv ${input_dir}${in_file} "${input_dir}0${in_file}"
  ## Copying test files
  cp ${test_dir}/Ref_files/REF-${in_file} ${input_dir}${in_file}
done

## Executing program
python3 ${src_dir}/main.py ${input_dir}${in_files[0]}

## Checking output files
##-----------------------
echo ""
echo "#--- Checking created output files ---#"
echo ""


## Looping over different output files 
for out_file in ${out_files[@]} ; do
  
  ## Variables for produced output file
  outp_file="${output_dir}${out_file}"
  ## Variables for test output file
  test_file="${test_dir}Ref_files/REF-${out_file}"

  ## Checking if output files have been produced
  if [ -f ${outp_file} ]; then
    echo " Found output file ${outp_file}"
  else
    echo " /!\\ Expected output file NOT FOUND: ${outp_file}"
    echo ""
    echo "     #################################"
    echo "     # /!\\ Execution test FAILED /!\\ #"
    echo "     #################################"
    echo ""
    exit 1 #Exit script with an error code
  fi
  
  ## Comparing produced output file to corresponding REF file
  if cmp -s "${outp_file}" "${test_file}"; then
    echo " ... identical to  ${test_file}"
    ## Removing output file created during the test 
    rm ${outp_file}
    echo "   (-> removed created output file)"
    echo ""
  else
    echo " ...DIFFERENT from ${test_file}:"
    echo ""
    echo "     #################################"
    echo "     # /!\\ Execution test FAILED /!\\ #"
    echo "     #################################"
    echo ""
    exit 1 #Exit script with an error code
  fi
done

## Removing created status file
rm "${output_dir}status.yaml" 

## Restoring original input files
for in_file in ${in_files[@]} ; do
  mv "${input_dir}0${in_file}" ${input_dir}${in_file}
done

##-------------##
## END MESSAGE ##
##-------------##
echo "      ###############################"
echo "      #> Execution test SUCCESSFUL <#"
echo "      ###############################"
echo ""

exit 0 #Exit the script with a succes code
