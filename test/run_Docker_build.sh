#!/bin/bash

#####
# Test script to demonstrate:
#  - Docker image building
#
# Written by J. Jahan
#####

## Set the errexit shell option to abort upon any error 
set -e

## Change to main directory of the repository
cd "$(dirname "$(readlink -f "$0")")"/../

##---------------##
## START MESSAGE ##
##---------------##
echo ""
echo "!------------------------------------------!"
echo "!  BQS EoS Module - Building Docker image  !"
echo "!------------------------------------------!"
echo "Starting..."

## Initialize directories needed for Docker volumes
input_dir="$(pwd)/input/"
output_dir="$(pwd)/output/"

## Building Docker image
##-----------------------
echo ""
echo "#--- Building Docker image ---#"
echo ""
docker build . -t eos_bqs-image:test

## Deleting intermediary images
##------------------------------
echo ""
echo "#--- Deleting intermediary-stage built images ---#"
echo ""
docker image prune -f

## Displaying test image created
##-------------------------------
echo ""
echo "#---Displaying created Docker image ---#"
echo ""
docker images eos_bqs-image

##-------------##
## END MESSAGE ##
##-------------##
echo ""
echo "      ###################################"
echo "      #> Docker image build SUCCESSFUL <#"
echo "      ###################################"
echo ""

exit 0 #Exit the script with a succes code
