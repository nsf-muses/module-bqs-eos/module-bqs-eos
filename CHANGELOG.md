# Changelog


## v1.0.1

### Added
- Added page in documentation regarding the stability scan of the equation of state with the current parametrization

## v1.0.0

### Added
- Started a Changelog
