
#####
# Building the Docker image for the MUSES BQS-EoS module
# Written by J. Jahan
#####

#--------------------------------------------------------#
# 1.Building 1st stage using the MUSES-common base image #
#--------------------------------------------------------#
FROM registry.gitlab.com/nsf-muses/common/muses-common:1.1.0 AS build
# RUN git clone --branch porter-v0.1.0 --single-branch https://gitlab.com/nsf-muses/common.git

# Set up environement
#---------------------
ARG UID=1000

# Copy source code
#------------------
WORKDIR /tmp
COPY --chown=$UID:$UID ./src/     src/
COPY --chown=$UID:$UID ./include/ include/
COPY --chown=$UID:$UID ./api/     api/
COPY --chown=$UID:$UID ./Makefile Makefile

# Compile the code
#------------------
WORKDIR /tmp/
RUN make redo

#-----------------------------------------------#
# 2. Building stage for actual BQS Docker image #
#-----------------------------------------------#
FROM python:3.11-slim

# Set up environement
#---------------------
ARG USERNAME=bqs
ARG UID=1000

## Install Git
RUN apt-get update && apt-get install -y git

## Create user with desired UID
RUN useradd --uid $UID --no-log-init --no-create-home --home-dir /opt --shell /bin/bash $USERNAME
RUN chown -R $UID:$UID /opt
USER $USERNAME

## Install Python dependencies
COPY --chown=$UID:$UID requirements.txt /tmp/
RUN pip install --user -r /tmp/requirements.txt

## Copy from previous "build" stage. This comes after the dependencies installation
## because iteration on the compilation is more likely than dependency list modifications
COPY --from=build --chown=$UID:$UID /tmp/ /opt/

# Copy from source code
#-----------------------
COPY --chown=$UID:$UID ./input/    /opt/input/
COPY --chown=$UID:$UID ./test/     /opt/test/

WORKDIR /opt
