#Repositories
INC_DIR=include/
SRC_DIR=src/

#Executable name
EXEC=EoS_BQS

#Header files
HFILES= ${INC_DIR}Functions.h  ${INC_DIR}nrD.h  ${INC_DIR}nrutilD.h  ${INC_DIR}Variables.h

#C Files 
CFILES= ${SRC_DIR}EoS_BQS.c  ${SRC_DIR}Functions.c  ${SRC_DIR}nrutilD.c  ${SRC_DIR}newtD.c \
        ${SRC_DIR}fdjacD.c  ${SRC_DIR}fminD.c  ${SRC_DIR}lnsrchD.c \
        ${SRC_DIR}lubksbD.c  ${SRC_DIR}ludcmpD.c \

#-------------------#
# Makefile commands #
#-------------------#
EoS_BQS:
	gcc ${HFILES} ${CFILES} -o ./${EXEC} -lm

run:
	./${EXEC} input/Coefficients_Parameters.dat input/Ranges_Parameters.dat

clean:
	rm ${EXEC} -f

redo:
	make clean; make

