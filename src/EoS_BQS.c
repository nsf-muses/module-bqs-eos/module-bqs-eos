/* 
	Copyright (c) 2019, Paolo Parotto and Jamie Stafford, 
	Department of Physics, University of Houston, Houston, TX 77204, US.
*/

/*
	This file produces a Taylor expanded EoS using Lattice QCD coefficients at muB=0.
*/ 

#define NRANSI

/* Import standard libraries. */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include "../include/nrD.h"
#include "../include/nrutilD.h"

/* Import additional library for many variables used in the program. */
#include "../include/Variables.h"
#include "../include/Functions.h"

/* Strangeness neutrality. */
#define NSN 2


/* Time variables for timing. */
clock_t start, end;
double cpu_time_used;

/* Giving double variables to Input matrix coefficients. */
float T_min, T_max, T_grid;
float muB_min, muB_max, muB_grid;
float muQ_min, muQ_max, muQ_grid;
float muS_min, muS_max, muS_grid;

/* Functions whose zeroes we are seeking for strangeness neutrality. */
void funcvSN(int n,double x[],double f[]){
	f[1] = StrDensTaylor(Tval,muBval,x[1],x[2]);
	f[2] = ChDensTaylor(Tval,muBval,x[1],x[2]) - 0.4*BarDensTaylor(Tval,muBval,x[1],x[2]);
} 


/* The main body of the program. */
int main(int argc, char *argv[])
{
	char buff[FILENAME_MAX];

	/* Define time variables to measure time elapsed creating the tables.*/
	time_t start, stop;

	/* All vectors and matrices are initialized. */
	/* Vectors for coefficients. */
	// Order 0
	CHI000PAR=vector(1,21);
	// Order 2
	// Diagonal 
	CHI200PAR=vector(1,21); CHI020PAR=vector(1,21); CHI002PAR=vector(1,21); 
	// Mixed
	CHI110PAR=vector(1,21);	CHI101PAR=vector(1,21);	CHI011PAR=vector(1,21);	
	// Order 4
	// Diagonal
	CHI400PAR=vector(1,21);	CHI040PAR=vector(1,21);	CHI004PAR=vector(1,21);
	// Mixed 31
	CHI310PAR=vector(1,21);	CHI301PAR=vector(1,21);	CHI031PAR=vector(1,21);	
	// Mixed 13
	CHI130PAR=vector(1,21);	CHI103PAR=vector(1,21); CHI013PAR=vector(1,21);	
  // Mixed 22
  CHI220PAR=vector(1,21); CHI202PAR=vector(1,21); CHI022PAR=vector(1,21);	
  // Mied 112
  CHI211PAR=vector(1,21);	CHI121PAR=vector(1,21); CHI112PAR=vector(1,21);

	/* Matrix for coefficients: 22 coefficients, 21 parameters each. */
	parMatrix=matrix(1,22,1,21);
	
	/* For strangeness neutrality */
	xSN=vector(1,NSN);
 	fSN=vector(1,NSN);
	
  /* Printing runstart message */
  printf("\n #~~~ BQS EoS code - Taylor expansion ~~~#\n\n");
    
	/* Assign the name of the main folder where the program lives and the files we wish to import are located.*/
  // Get current working directory 
	getcwd(buff,FILENAME_MAX);
	printf("Current working directory is: %s \n\n",buff);
  // Get path of executable relative to current working directory 
  char *rltv_path = argv[0];
  rltv_path[strlen(rltv_path)-7] = '\0'; //remove last seven characters (='EoS_BQS')
    
  /* Define path where output files will be stored */
  char *out_path = strcat(rltv_path, "output/");

  /* Start clock for importing lists */
  start = clock();

	/* Parametrization parameters are read from the user-input file, and saved. */
  FILE *ParametersIn = fopen(argv[1], "r");
  if (ParametersIn == 0){
    fprintf(stderr,"failed to open paremeters file\n");
    exit(1);
  }
  for(i=1;fscanf(ParametersIn,"%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", &A0, &A1, &A2, &A3, &A4, &A5, &A6, &A7, 
                                                                                  &A8, &A9, &B0, &B1, &B2, &B3, &B4, &B5, &B6, &B7, &B8, &B9, &C0) !=EOF; i++){
    parMatrix[i][1] = A0;  	    parMatrix[i][2] = A1;  	    parMatrix[i][3] = A2;  	    parMatrix[i][4] = A3;
    parMatrix[i][5] = A4;  	    parMatrix[i][6] = A5;  	    parMatrix[i][7] = A6;  	    parMatrix[i][8] = A7;
    parMatrix[i][9] = A8;  	    parMatrix[i][10] = A9;
    parMatrix[i][11] = B0;  	  parMatrix[i][12] = B1;  	  parMatrix[i][13] = B2;      parMatrix[i][14] = B3;
    parMatrix[i][15] = B4;  	  parMatrix[i][16] = B5;  	  parMatrix[i][17] = B6;  	  parMatrix[i][18] = B7;
    parMatrix[i][19] = B8;  	  parMatrix[i][20] = B9;
    parMatrix[i][21] = C0;
      
  // ~ printf("%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",A0,A1,A2,A3,A4,A5,A6,A7,B0,B1,B2,B3,B4,B5,B6,B7,C0);
  }
  fclose(ParametersIn);	
  	
	// Vectors for coefficients.   
	// All vectors are filled with parameters. 
	//--- Order 0 ---
    for(i=1;i<=21;i++) CHI000PAR[i] = parMatrix[1][i];
    //--- Order 2 ---
    // Diagonal 
    for(i=1;i<=21;i++) CHI200PAR[i] = parMatrix[2][i];
    for(i=1;i<=21;i++) CHI020PAR[i] = parMatrix[3][i];
    for(i=1;i<=21;i++) CHI002PAR[i] = parMatrix[4][i];
	// Mixed
	for(i=1;i<=21;i++) CHI110PAR[i] = parMatrix[5][i];
    for(i=1;i<=21;i++) CHI101PAR[i] = parMatrix[6][i];
    for(i=1;i<=21;i++) CHI011PAR[i] = parMatrix[7][i];
	//--- Order 4 ---
    // Diagonal	
    for(i=1;i<=21;i++) CHI400PAR[i] = parMatrix[8][i];
    for(i=1;i<=21;i++) CHI040PAR[i] = parMatrix[9][i];
    for(i=1;i<=21;i++) CHI004PAR[i] = parMatrix[10][i];
    // Mixed 31
    for(i=1;i<=21;i++) CHI310PAR[i] = parMatrix[11][i];
    for(i=1;i<=21;i++) CHI301PAR[i] = parMatrix[12][i];
    for(i=1;i<=21;i++) CHI031PAR[i] = parMatrix[13][i];
    // Mixed 13
    for(i=1;i<=21;i++) CHI130PAR[i] = parMatrix[14][i];
    for(i=1;i<=21;i++) CHI103PAR[i] = parMatrix[15][i];
    for(i=1;i<=21;i++) CHI013PAR[i] = parMatrix[16][i];
    // Mixed 22
    for(i=1;i<=21;i++) CHI220PAR[i] = parMatrix[17][i];
    for(i=1;i<=21;i++) CHI202PAR[i] = parMatrix[18][i];
    for(i=1;i<=21;i++) CHI022PAR[i] = parMatrix[19][i];
    // Mixed 211
    for(i=1;i<=21;i++) CHI211PAR[i] = parMatrix[20][i];
    for(i=1;i<=21;i++) CHI121PAR[i] = parMatrix[21][i];
    for(i=1;i<=21;i++) CHI112PAR[i] = parMatrix[22][i];
  	
	/* Matrix for the user input values of T,muB,muS,muQ. */
	float matrix[4][3];

  /* Thermodynamic parameters are read from the user-input file, and saved. */
	FILE *fp;
	fp = fopen(argv[2], "r");
  	
	/* Check if file is opened correctly. */
  if (fp == 0){
    fprintf(stderr,"failed to open paremeters file\n");
    exit(1);
	}

	/* Read Matrix elements from file using fscanf. */
	for (int i = 0; i < 4 ; i++){
		for (int j = 0; j < 3; j++){
			fscanf(fp, "%f", &matrix[i][j]);
		}
	}
	
	/* Assign the read values to variables. */
	T_min = matrix[0][0];
	T_max = matrix[0][1];
	T_grid = matrix[0][2];
	muB_min = matrix[1][0];
	muB_max = matrix[1][1];
	muB_grid = matrix[1][2];
	muQ_min = matrix[2][0];
	muQ_max = matrix[2][1];
	muQ_grid = matrix[2][2];
	muS_min = matrix[3][0];
	muS_max = matrix[3][1];
	muS_grid = matrix[3][2];
	
	/* Close the file. */
	fclose(fp);
  	
  /* Create folder for coefficients checks. */
	mkdir(out_path, S_IRWXU | S_IRWXG | S_IRWXO);
	chdir(out_path);
  	
  // Print values of all coefficients and derivatives thereof wrt T. (To check that everything is in order.)
  if(muB_min == 0.0 && muQ_min == 0.0 && muS_min == 0.0){
    FILE *CHIS = fopen("All_Chis.dat","w");
    FILE *DCHISDT = fopen("All_DChisDT.dat","w");
    FILE *D2CHISDT2 = fopen("All_D2ChisDT2.dat","w");
    for(float t=T_min; t<=T_max; t+=T_grid){
        fprintf(CHIS,"%f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f\n",
                    t,CHI000(t),CHI200(t),CHI020(t),CHI002(t),CHI110(t),CHI101(t),CHI011(t),CHI400(t),CHI040(t),CHI004(t),CHI310(t),CHI301(t),CHI031(t),CHI130(t),CHI103(t),CHI013(t),
                    CHI220(t),CHI202(t),CHI022(t),CHI211(t),CHI121(t),CHI112(t));
        fprintf(DCHISDT,"%f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f\n",
                    t,DCHI000DT(t),DCHI200DT(t),DCHI020DT(t),DCHI002DT(t),DCHI110DT(t),DCHI101DT(t),DCHI011DT(t),DCHI400DT(t),
                    DCHI040DT(t),DCHI004DT(t),DCHI310DT(t),DCHI301DT(t),DCHI031DT(t),DCHI130DT(t),DCHI103DT(t),DCHI013DT(t),
                    DCHI220DT(t),DCHI202DT(t),DCHI022DT(t),DCHI211DT(t),DCHI121DT(t),DCHI112DT(t));
        fprintf(D2CHISDT2,"%f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f\n",
                  t,D2CHI000DT2(t),D2CHI200DT2(t),D2CHI020DT2(t),D2CHI002DT2(t),D2CHI110DT2(t),D2CHI101DT2(t),D2CHI011DT2(t),D2CHI400DT2(t),
                    D2CHI040DT2(t),D2CHI004DT2(t),D2CHI310DT2(t),D2CHI301DT2(t),D2CHI031DT2(t),D2CHI130DT2(t),D2CHI103DT2(t),D2CHI013DT2(t),
                    D2CHI220DT2(t),D2CHI202DT2(t),D2CHI022DT2(t),D2CHI211DT2(t),D2CHI121DT2(t),D2CHI112DT2(t));            
    }
    fclose(CHIS);  	
    fclose(DCHISDT);
    fclose(D2CHISDT2);
  }

  /* Thermodynamics for ranges in T, muB, muS and muQ required by user. */  	
  FILE *All_Therm_Taylor = fopen("EoS_Taylor_AllMu.dat","w");
  FILE *All_Therm_Der = fopen("EoS_Taylor_AllMu_Derivatives.dat","w");
  for(i=T_min;i<=T_max;i+=T_grid){ 
    for(j=muB_min;j<=muB_max;j+=muB_grid){
      for(k=muQ_min;k<=muQ_max;k+=muQ_grid){
        for(l=muS_min;l<=muS_max;l+=muS_grid){
          Tval = i; muBval = j;  muQval = k; muSval = l;
            
          ///Dimensionless quantities  
          // ~ /* Thermodynamics variables (dimensionless). */
          // ~ PressVal = PressTaylor(i,j,k,l);
          // ~ EntrVal = EntrTaylor(i,j,k,l);
          // ~ BarDensVal = BarDensTaylor(i,j,k,l);
          // ~ StrDensVal = StrDensTaylor(i,j,k,l);
          // ~ ChDensVal = ChDensTaylor(i,j,k,l);
          // ~ EnerDensVal = EntrVal - PressVal + muBval/Tval*BarDensVal + muQval/Tval*ChDensVal+ muSval/Tval*StrDensVal;
          // ~ SpSoundVal = SpSound(Tval,muBval,muQval,muSval);
          
          // ~ /* Second Order Derivatives. */
          // ~ D2PB2 = P2B2(i,j,k,l);
          // ~ D2PQ2 = P2Q2(i,j,k,l);
          // ~ D2PS2 = P2S2(i,j,k,l);
          
          // ~ D2PBQ = P2BQ(i,j,k,l);
          // ~ D2PBS = P2BS(i,j,k,l);
          // ~ D2PQS = P2QS(i,j,k,l);
        
          // ~ D2PTB = P2TB(i,j,k,l);
          // ~ D2PTQ = P2TQ(i,j,k,l);
          // ~ D2PTS = P2TS(i,j,k,l);
          // ~ D2PT2 = P2T2(i,j,k,l);

          ///Dimension-full quantities  
          /* Thermodynamics variables (dimensionfull). */
          PressVal = PressTaylor(i,j,k,l) * Tval*Tval*Tval*Tval;
          EntrVal = EntrTaylor(i,j,k,l) * Tval*Tval*Tval;
          BarDensVal = BarDensTaylor(i,j,k,l) * Tval*Tval*Tval;
          StrDensVal = StrDensTaylor(i,j,k,l) * Tval*Tval*Tval;
          ChDensVal = ChDensTaylor(i,j,k,l) * Tval*Tval*Tval;
          EnerDensVal = Tval * EntrVal - PressVal + muBval*BarDensVal + muQval*ChDensVal+ muSval*StrDensVal;
          SpSoundVal = SpSound(Tval,muBval,muQval,muSval);
            
          /* Second Order Derivatives. */
          D2PB2 = P2B2(i,j,k,l) * Tval*Tval;
          D2PQ2 = P2Q2(i,j,k,l) * Tval*Tval;
          D2PS2 = P2S2(i,j,k,l) * Tval*Tval;
          
          D2PBQ = P2BQ(i,j,k,l) * Tval*Tval;
          D2PBS = P2BS(i,j,k,l) * Tval*Tval;
          D2PQS = P2QS(i,j,k,l) * Tval*Tval;
        
          D2PTB = P2TB(i,j,k,l) * Tval*Tval;
          D2PTQ = P2TQ(i,j,k,l) * Tval*Tval;
          D2PTS = P2TS(i,j,k,l) * Tval*Tval;
          D2PT2 = P2T2(i,j,k,l) * Tval*Tval;

          fprintf(All_Therm_Taylor,"%lf  %lf  %lf  %lf  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f\n", Tval, muBval, muQval, muSval, PressVal, EntrVal, 
                  BarDensVal, StrDensVal, ChDensVal, EnerDensVal, SpSoundVal);
          fprintf(All_Therm_Der,"%lf  %lf  %lf  %lf  %3.12f  %3.12f %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f  %3.12f\n", Tval, muBval, muQval, muSval, D2PB2, D2PQ2, D2PS2, D2PBQ, D2PBS, D2PQS,
                  D2PTB, D2PTQ, D2PTS, D2PT2);
        }
      }	    
    }
	}    
	fclose(All_Therm_Taylor);
    fclose(All_Therm_Der);
	
	chdir(buff);

	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("Finished calculating in %lf seconds.\n", cpu_time_used);
    printf("Output files are located in: %s \n\n", out_path);	
	
	return 0;
}


#undef NRANSI
