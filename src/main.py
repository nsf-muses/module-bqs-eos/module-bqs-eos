#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-----------------------------------------------------------------------------------------------
#Script writen by J.Jahan (johannes.jahan@gmail.com), last update 2024/07/10
#version: 1.0.0
#-----------------------------------------------------------------------------------------------
# |BQS EoS Module|
# ----------------
#	Main script called to run the module.
# Relies on adapter functions defined in:
# - Input_adapter-YAML_to_dat.py
# - Output_adapter-dat_to_YAML.py
# 
# Inputs are the YAML files containing parameters, taken as arguments when calling the script.
# The order in wich inputs are provided does not matter.
# 
#  Ex.: 
#   $> python3 main.py ./input/config.yaml ./input/BQS_eos_input_coef.yaml
#
# See API specs about the YAML structure for input/output files in: 
#   ./api/OpenAPI_Specifications-BQS_EoS.yaml
################################################################################################

import sys as sys
import os as os
import yaml as yaml
import subprocess as sub

from muses_porter import Porter

#Determine current path to define all other paths relatives to it
pwd = os.path.abspath(os.path.dirname(__file__))
home_path = pwd[:pwd.rindex('/')+1]
api_path = os.path.join(home_path, 'api/')
inp_path = os.path.join(home_path, 'input/')
out_path = os.path.join(home_path, 'output/')
src_path = os.path.join(home_path, 'src/')
tst_path = os.path.join(home_path, 'test/Ref_files/')

#Add path to the libraries with converting functions
sys.path.append(api_path)
from Input_adapter_YAML_to_dat import *
from Output_adapter import *
from OpenAPI_Specifications_validator import *

#--------#
# INPUTS #
#--------#


#Affects the list of arguments given in the command line
args = sys.argv[1:]

# Ensuring the executable is here 
#---------------------------------
if not os.path.exists(os.path.join('.',home_path,'EoS_BQS')):
    #If absent file create 'status.yaml' with error code + message and stop module execution
    with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
        yaml.dump({"code":500, "message":"[main.py]> Executable of the code '" + home_path + "EoS_BQS' not found."}, outfile, default_flow_style=False)
    #Exit with error message
    sys.exit("[main.py]> Executable of the code '" + home_path + "EoS_BQS' not found \n\nOPERATION ABORTED")

# Ensuring input files are found
#--------------------------------
for arg in args:
    #If absent file...
    if not os.path.exists(arg):
        #...create 'status.yaml' with error code + message and stop module execution
        with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
            yaml.dump({"code":400, "message":"[main.py]> File '" + arg + "' not found."}, outfile, default_flow_style=False)
        sys.exit("[main.py]> File '" + arg + "' not found \n\nOPERATION ABORTED")

# Ensuring inputs are valid
#---------------------------
#Expected input files
specifications = os.path.join(api_path, 'OpenAPI_Specifications-BQS_EoS.yaml')

#Extract specifications from file
with open(specifications, 'r') as spec_file:
    openapi_specs = Spec.from_file(spec_file)

#Dictionnary to store information on input files from specifications
spec_input_files = {}

#Collecting information on input files in specifications
for path in openapi_specs["paths"].keys():
    #Check if 'path' correspond to an input file
    if "input" in path:
        #Add it to the spec_input_files{} dict. and indicate if required + add call marker
        spec_input_files[path[1:]] = {"required": openapi_specs["paths"][path]["put"]["requestBody"]["required"], "called": False, "copied": False}

#Looping over arguments of the script
for arg in args:
    #Validate input file against module's OpenAPI specifications 
    valid_input = OpenAPI_input_validator(specifications, arg, verbose=True)
    #Loop over input files from specs
    for input_file in spec_input_files.keys():
        #Mark file as called if positive
        if input_file in arg:
            spec_input_files[input_file]["called"] = True
    #Assign name of new validated + unmarshaled input files
    if "config.yaml" in arg: 
        input_user = valid_input
    elif "_coef.yaml" in arg:
        input_coef = valid_input

#Loop over input files info from specs
for input_file in spec_input_files.keys():
    #If required file not called with the script...
    if spec_input_files[input_file]["required"] == True and spec_input_files[input_file]["called"] == False:
        #...create 'status.yaml' with error code + message and stop module execution
        with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
            yaml.dump({"code":400, "message":"[main.py]> Missing required file: " + input_file}, outfile, default_flow_style=False)
        sys.exit("[main.py]> Missing required file: " + input_file + "\n\nOPERATION ABORTED")
    #If non-required file is not called explicitly
    elif spec_input_files[input_file]["required"] == False and spec_input_files[input_file]["called"] == False:
        #Create name for reference file
        filemark = input_file.rfind('/')
        ref_file = os.path.join(tst_path, "REF-"+input_file[filemark+1:])
        #Make sure reference file is found
        if not os.path.exists(ref_file):
            #If absent file create 'status.yaml' with error code + message and stop module execution
            with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
                yaml.dump({"code":500, "message":"[main.py]> Reference file for optionatl input '" + input_file + " not found in " + tst_path + "."}, outfile, default_flow_style=False)
            #Exit with error message
            sys.exit("[main.py]> Reference file for optionatl input '" + input_file + " not found in " + tst_path + " \n\nOPERATION ABORTED")
        #Copy corresponding REF file from 'test/' repository
        sub.call(["cp", ref_file, os.path.join(home_path, input_file)])
        spec_input_files[input_file]["copied"] = True
        #Validate input file against module's OpenAPI specifications
        valid_input = OpenAPI_input_validator(specifications, os.path.join(home_path, input_file), verbose=True)
        #Assign name of new validated + unmarshaled input files
        input_coef = valid_input

#---------------#
# INPUT ADAPTER #
#---------------#

#Defining a dico of switchers to turn on/off the recording of output variables
switchers = {}

# Calling the function to get the .dat input for the code from the YAML inputs
#------------------------------------------------------------------------------
Input_YAML_to_dat(inp_path, input_user, input_coef, specifications, switchers)

#------------------#
# RUNNING THE CODE #
#------------------#

code_exec = sub.call([os.path.join(".", home_path, "EoS_BQS"), os.path.join(inp_path, "Coefficients_Parameters.dat"), os.path.join(inp_path, "Ranges_Parameters.dat")])

#Print out status file with error in case of problem in the code execution 
if code_exec != 0:
    with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
        yaml.dump({"code":500, "message":"[EoS_BQS]> Error while executing EoS_BQS code."}, outfile, default_flow_style=False)
    sys.exit("[EoS_BQS]> Error while executing EoS_BQS code\n\nOPERATION ABORTED")
            
#----------------#
# OUTPUT ADAPTER #
#----------------#

# Calling the function to get the .dat input for the code from the YAML inputs
#------------------------------------------------------------------------------
code_exec = Output_dat_to_CSV(out_path, specifications, switchers)

#Print out status file with error in case of problem in the converter execution 
if code_exec != 0:
    with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
        yaml.dump({"code":500, "message":"[EoS_BQS]> Error while executing EoS_BQS code."}, outfile, default_flow_style=False)
    sys.exit("[EoS_BQS]> Error while executing EoS_BQS code\n\nOPERATION ABORTED")

#--------------------------#
# DELETING TEMPORARY FILES #
#--------------------------# 

#Removing '__pycache__' repository
sub.call(["rm", "-rf", os.path.join(api_path, "__pycache__")])

#Moving valid input files in output directory
for input_file in [input_user, input_coef]:
    sub.call(["mv", os.path.join(inp_path, input_file), out_path])

#Removing intermediary input files
input_files = os.listdir(inp_path)
for input_file in input_files:
    if ".dat" in input_file:
        os.remove(os.path.join(inp_path, input_file))
    for spec_input in spec_input_files.keys():
        if input_file in spec_input and spec_input_files[spec_input]["copied"] == True:
            os.remove(os.path.join(inp_path, input_file))

#Removing intermediary output files
output_files = os.listdir(out_path)
for dat_file in output_files:
    if ".dat" in dat_file: 
        os.remove(os.path.join(out_path, dat_file))
        
#----------------------------------------------#
# WRITING STATUS FILE FOR SUCCESSFUL OPERATION #
#----------------------------------------------#
with open(os.path.join(out_path, "status.yaml"), 'w') as outfile:
    yaml.dump({"code":200, "message":"Succcessful module execution"}, outfile, default_flow_style=False)
    
# Validating status.yaml output
#-------------------------------
status_file = os.path.join(out_path, "status.yaml") 
valid_input = OpenAPI_input_validator(specifications, status_file, print_valid=False)
