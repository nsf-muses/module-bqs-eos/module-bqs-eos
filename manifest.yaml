name:
  short_name: bqs_eos
  display_name: Taylor expanded BQS Equation of State
authors:
  - Johannes Jahan
  - Hitansh Shah
  - Paolo Parotto
  - Jamie Karthein
  - Claudia Ratti

## The `docker run` command to be executed in the container
command:
  - 'python3'
  - './src/main.py'
  - './input/config.yaml'

## Specify the location of the container image for `docker pull`
image:
  registry: "registry.gitlab.com"
  repo: "nsf-muses/module-bqs-eos/module-bqs-eos"
  tag: "v0.9.2"
  source:
    url: "https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos"
    git: "https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos.git"
    path: Dockerfile
    targetRevision: "v0.9.2"

## Module documentation metadata
docs:
  ## Relative path to the Sphinx-compatible documentation source files
  path: "docs/src"

## Input and output file descriptions
## Labels must be unique across both inputs and outputs. Reserved labels are
##     "config" and "status" used for the required input and output files.
inputs:
  - label: config
    description: "Configuration of the calculation ranges and desired output to be recorded"
    path: "/opt/input/config.yaml"
    api:
      url: "https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/9adc75c4cdf906df9cd627262eadf409a63e9475/input/config.yaml"
      targetRevision: ""
      path: /opt/api/OpenAPI_Specifications-BQS_EoS.yaml#/components/schemas/Input_BQS_EoS_user

  - label: coef_input
    description: "List of coefficients used to parametrise susceptibilities"
    path: "/opt/input/BQS_eos_input_coef.yaml"
    api:
      url: "https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/9f97b69ba3efd5c72b72fbeb5ae7142a6f521fae/input/BQS_eos_input_coef.yaml"
      targetRevision: ""
      path: /opt/api/OpenAPI_Specifications-BQS_EoS.yaml#/components/schemas/Taylor_coefficients_parameters

outputs:
  ## The "status" output file is required. The Calculation Engine will parse
  ## this file to determine the status of execution and to relay any error
  ## messages to the user.
  - label: status
    description: "Status code and comments regarding module's execution."
    path: "/opt/output/status.yaml"
    api:
      url: ""
      targetRevision: ""
      path: /opt/api/OpenAPI_Specifications-BQS_EoS.yaml#/components/schemas/status

  - label: equation_of_state_thermo
    description: "Thermodynamics results for the calculated equation of state."
    path: "/opt/output/BQS_eos_output_thermodynamics.csv"
    api:
      url: ""
      targetRevision: ""
      path: /opt/api/OpenAPI_Specifications-BQS_EoS.yaml#/components/schemas/EoS_thermodynamics

  - label: equation_of_state_thermo-metadata
    description: "Metadata for file containing thermodynamics results"
    path: "/opt/output/BQS_eos_metadata_thermodynamics.yaml"

  - label: equation_of_state_chis
    description: "Susceptibilities with their 1st and 2nd order T-derivatives at T=0."
    path: "/opt/output/BQS_eos_output_chis.csv"
    api:
      url: ""
      targetRevision: ""
      path: /opt/api/OpenAPI_Specifications-BQS_EoS.yaml#/components/schemas/EoS_chis_derivatives

  - label: equation_of_state_chis-metada
    description: "Metadata for file containing usceptibilities with their 1st and 2nd order T-derivatives at T=0."
    path: "/opt/output/BQS_eos_metadata_chis.yaml"

