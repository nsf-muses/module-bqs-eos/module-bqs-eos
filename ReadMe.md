4D Taylor-expanded lattice (BQS)
================================

The so-called BQS EoS (Equation of State) module of the NSF-funded [MUSES](https://musesframework.io/) project enables to calculate a 4D equation of state of nuclear matter, at finite temperature $T$ and finite baryon number, electric charge and strangeness chemical potentials $\mu_B$, $\mu_Q$ and $\mu_S$, respectively.
It is based on a 3-dimensional Taylor expansion of lattice QCD data computed at zero chemical potentials.

It can be used to compute a nuclear equation of state for a temperature range of $30 < T < 800$ MeV, and for chemical potential values of $0 < \mu_i < 450$ MeV (with $i=B/Q/S$).

**Original source code was developed by:** Paolo Parotto$^{(1)}$ & Jamie Stafford$^{(1)}$.  
**Module development and code adaptation by:** Johannes Jahan $^{(1)}$ & Hitansh Shah $^{(1)}$.

_$^{(1)}$ Department of Physics, University of Houston, Houston, TX 77204, USA._

============================

For complete documentation on the physics and how to use this module, see [MUSES online documentation](https://musesframework.io/docs/modules/eos_taylor_4d/Index.html).
