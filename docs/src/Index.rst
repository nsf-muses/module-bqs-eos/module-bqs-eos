4D Taylor-expanded lattice (BQS)
=================================

The so-called BQS EoS (Equation of State) module of the NSF-funded
`MUSES <https://musesframework.io/>`__ project enables to calculate a 4D
equation of state of nuclear matter, at finite temperature :math:`T` and
finite baryon number, electric charge and strangeness chemical
potentials :math:`\mu_B`, :math:`\mu_Q` and :math:`\mu_S`, respectively.
It is based on a 3-dimensional Taylor expansion of lattice QCD data
computed at zero chemical potentials.

It can be used to compute a nuclear equation of state for a temperature
range of :math:`30 < T < 800` MeV, and for chemical potential values of
:math:`0 < \mu_i < 450` MeV (with :math:`i=B/Q/S`).

| **When using results from this module for scientific publication, please cite:**  
| J. Noronha-Hostler, P. Parotto, C. Ratti, and J. M. Stafford, *Phys.Rev.C 100, 064910* (2019), `arXiv:1902.06723 <https://arxiv.org/abs/1902.06723>`__ [hep-ph]

============================

| **Original source code was developed by:** Paolo Parotto\ :math:`^{(1)}`,
  Jamie Stafford\ :math:`^{(1)}` &
  Jacquelyn Noronha-Hostler\ :math:`^{(1)}`.
| **Module development and code adaptation by:** Johannes Jahan\ :math:`^{(1)}` 
  & Hitansh Shah\ :math:`^{(1)}`.

| :math:`^{(1)}` *Department of Physics, University of Houston, Houston, TX 77204, USA.* 

============================

.. toctree::
   :maxdepth: 1
   :caption: Contents
   :glob:

   _contents/physics.rst
   _contents/stability.rst
   _contents/quickstart.rst
   _contents/units.rst
   _contents/parameters.rst
   _contents/code_structure.rst
   _contents/troubleshooting.rst


