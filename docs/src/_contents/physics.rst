Physics overview
----------------

The present code provides a four dimensional Taylor-expanded QCD
equation of state (EoS) as a function of temperature :math:`T` and
baryon, strangeness and electric charge chemical potential
:math:`\mu_B`, :math:`\mu_S` and :math:`\mu_Q`, respectively. At
:math:`\mu_i = 0`, where :math:`i = B/Q/S`, we use the equation of state
produced by lattice QCD simulations, and perform a Taylor expansion in
terms of :math:`\frac{\mu_i}{T}` in order to go to finite value of these chemical
potentials. Susceptibilities of :math:`B`, :math:`Q` and :math:`S` conserved charges at zero chemical
potential :math:`\chi_{ijk}^{BQS}(T,\mu_i=0)`, which play the role of
Taylor expansion coefficients, are described using a Padé functional
form fitted to lattice susceptibilities, up to :math:`4^{th}` order [1]_.

All lattice data used in this construction have been computed by the
Wuppertal-Budapest collaboration, from a :math:`N_\tau=12` lattice using
4stout action [2]_.

Taylor Expansion
~~~~~~~~~~~~~~~~

The Taylor series of the pressure in terms of the three conserved charge
chemical potentials is written as:

.. math::

   \frac{P(T,\mu_B,\mu_Q,\mu_S)}{T^4}= \sum_{i,j,k} \frac{1}{i!j!k!} \chi_{ijk}^{BQS}(\frac{\mu_B}{T})^i (\frac{\mu_Q}{T})^j (\frac{\mu_S}{T})^k

The first term, :math:`i = j = k = 0`, corresponds to the pressure
itself as calculated on the lattice (at zero chemical potentials), and the 
subsequent Taylor expansion coefficients are the conserved charge 
susceptibilities with appropriate factorial coefficients corresponding to 
their order. The susceptibilities are defined as derivatives of the QCD 
pressure with respect to the various conserved charge chemical potentials:

.. math::

   \chi_{ijk}^{BQS} =\left. \frac{\partial^{i+j+k}(P/T^4)}{\partial(\frac{\mu_B}{T})^i (\frac{\mu_Q}{T})^j (\frac{\mu_B}{T})^k}\right \vert_{\mu_B,\mu_Q,\mu_S = 0}

Lattice QCD simulations allow to compute these coefficients at several 
finite lattice spacing, to then extrapolate them to infinite volume. 
Here, we employ susceptibilities up to :math:`4^{th}` order, *i.e.*
susceptibilities for which :math:`i+j+k \leqslant 4`, with :math:`i+j+k`
even due to symmetry between baryonic and antibaryonic matter. Using the 
Taylor expansion and the parametrized coefficients, we obtain pressure 
and derive all other quantities from thermodynamic relations.

Parameterizations
~~~~~~~~~~~~~~~~~

The susceptibilities, or Taylor expansion coefficients, are parameterized 
to obtain a smooth description of each one of them over the entire temperature 
range :math:`30 \leqslant T \leqslant 800 MeV` [1]_. This allow to get pressure
as a smooth function of :math:`T`, :math:`\mu_B`, :math:`\mu_Q` and :math:`\mu_S`. 
In order to achieve it, each susceptibility is parametrized by means of a ratio 
of polynomials in :math:`1/T`, up to :math:`9^{th}` order:

.. math::

	\chi_{ijk}^{BQS} (T) = \\
	\; \; \frac{a_{0}+ a_{1}/x+ a_{2}/x^2 + a_{3}/x^3 + a_{4}/x^4 + a_{5}/x^5 + a_{6}/x^6 + a_{7}/x^7 + a_{8}/x^8 + a_{9}/x^9 }{b_{0}+ b_{1}/x+ b_{2}/x^2 + b_{3}/x^3 + b_{4}/x^4 + b_{5}/x^5 + b_{6}/x^6 + b_{7}/x^7 + b_{8}/x^8 + b_{9}/x^9 } + c_{0} \; ,

with :math:`a_{0-9}`, :math:`b_{0-9}` and :math:`c_0` being coefficients of the
parameterization, unique for each :math:`\chi_{ijk}^{BQS} (T)`. Only one exception applies to :math:`\chi_{2}^{B} (T)`, for which the optimal parametrization is given as:

.. math::

   \chi_2^{B} (T) = e^{-h_1/x' - h_2 / x'^2} \cdot f_3 \cdot (1+tanh(f_4 x' + f_5)) \; .

In these equations, :math:`x = T/154 MeV` and :math:`x' = T/200 MeV`. 
These correspond to normalized temperatures on which the fit functions depend. 
The normalization factor for the temperature in the polynomial parametrization
was chosen to be close to the crossover transition temperature. For 
:math:`\chi_2^{B}`, the best fit is obtained by normalizing around the point at
which the function begins to plateau.

These parametric forms are fitted to a combination of HRG calculations [3]_, 
for :math:`T<135` MeV, and lattice data of the Wuppertal-Budapest collaboration
obtained from simulations on :math:`48^3 \times 12` lattices [2]_, for 
:math:`135<T<220` MeV.
In order to obtain the correct extrapolation up to high :math:`T`, the value of each
different susceptibilities at :math:`T=800` MeV has been assumed to be :math:`10\%`
away from their respective Stefan-Boltzmann limit (*i.e.* their limit at 
:math:`T \to \infty`).


References
~~~~~~~~~~

.. [1] J. Noronha-Hostler, P. Parotto, C. Ratti, and J. M. Stafford, *Phys.Rev.C 100, 064910* (2019), `arXiv:1902.06723 <https://arxiv.org/abs/1902.06723>`__ [hep-ph]

.. [2] S. Borsanyi, Z. Fodor, J. N. Guenther, S. K. Katz, K. K. Szabo, A. Pasztor, I. Portillo, and C. Ratti, *JHEP 10, 205* (2018), `arXiv:1805.04445 <https://arxiv.org/abs/1805.04445>`__ [hep-lat]

.. [3] P. Alba, W. Alberico, R. Bellwied, M. Bluhm, V. Mantovani Sarti, M. Nahrgang, and C. Ratti,     *Phys.Lett.B 738* (2014) 305-310, `arXiv:1403.4903 <https://arxiv.org/abs/1403.4903>`__ [hep-ph]
