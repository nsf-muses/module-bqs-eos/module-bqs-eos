Limits of stability and causality
---------------------------------

The limits of the regions where the equation of state obtained through the 4D Taylor expansion, described in the `Physics overview <https://musesframework.io/docs/modules/eos_taylor_4d/_contents/physics.html>`__, is stable and causal have been studied. In this section, the conditions studied to determine the stability criteria are explicited, and the result obtained in some particular directions of finite chemicals potentials are shown.

Stability conditions
~~~~~~~~~~~~~~~~~~~~

To verify to stability of an equation of state depending on :math:`T`, :math:`\mu_B`, :math:`\mu_Q` and :math:`\mu_S`, one needs to make sure that the :math:`4 \times 4` Hessian matrix :math:`M` of pressure :math:`P(T, \mu_B, \mu_Q, \mu_S)` is positive definite [1]_: 

.. math::
    M=\begin{bmatrix}
        \frac{\partial s}{\partial T}\big|_{\vec{\mu}} & \frac{\partial s}{\partial \mu_B}\big|_{T,\mu_S,\mu_Q} & \frac{\partial s}{\partial \mu_S}\big|_{T,\mu_B,\mu_Q} & \frac{\partial s}{\partial \mu_Q}\big|_{T,\mu_B,\mu_S} \\
        \frac{\partial n_B}{\partial T}\big|_{\vec{\mu}} & \chi_2^B & \chi_{11}^{BS} & \chi_{11}^{BQ} \\
        \frac{\partial n_S}{\partial T}\big|_{\vec{\mu}} &   \chi_{11}^{SB} & \chi_2^S & \chi_{11}^{SQ} \\
        \frac{\partial n_Q}{\partial T}\big|_{\vec{\mu}} & \chi_{11}^{QB} & \chi_{11}^{QS}  & \chi_2^Q
    \end{bmatrix} \; .

This means that each submatrix should have a positive or null determinant.

For the :math:`3 \times 3` submatrix of conserved charges chemical potentials, it is equivalent to verify that:

.. math::
	\chi^B_2 \geq 0 \; , \\ 
	\chi^Q_2 \geq 0 \; , \\ 
	\chi^S_2 \geq 0 \; ,

.. math::
	\chi^B_2 \chi^Q_2 \geq (\chi^{BQ}_{11})^2 \; , \\
	\chi^B_2 \chi^S_2 \geq (\chi^{BS}_{11})^2 \; , \\
	\chi^Q_2 \chi^S_2 \geq (\chi^{QS}_{11})^2 \; ,

and 

.. math::
    \chi^B_2 \chi^Q_2 \chi^S_2 + 2 \left(\chi^{BQ}_{11} \chi^{BS}_{11} \chi^{QS}_{11}\right) \geq \chi^{B}_{2} \left(\chi^{QS}_{11}\right)^2 + \chi^{Q}_{2} \left(\chi^{BS}_{11}\right)^2 + \chi^{S}_{2} \left(\chi^{BQ}_{11}\right)^2 \; .

To verify the positivity of the entire :math:`4 \times 4` Hessian matrix, one has to take into account the additional terms involving :math:`T`, leading to the following additional conditions to verify:

.. math::
    \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}} \geq 0 \; ,


.. math:: 
    \chi_2^B \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}} \geq \left[\frac{\partial s}{\partial \mu_B}\bigg|_{T,\mu_S,\mu_Q}\right]^2 \; , \\
    \chi_2^S \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}} \geq \left[\frac{\partial s}{\partial \mu_S}\bigg|_{T,\mu_B,\mu_Q}\right]^2 \; , \\
    \chi_2^Q \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}} \geq \left[\frac{\partial s}{\partial \mu_Q}\big|_{T,\mu_B,\mu_S}\right]^2 \; ,


.. math::
    \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}}\left[\chi_2^B\chi_2^S-\left(\chi_{11}^{BS}\right)^2\right] + 2\chi_{11}^{BS}\frac{\partial s}{\partial \mu_B}\bigg|_{T,\mu_S,\mu_Q} \frac{\partial s}{\partial \mu_S}\bigg|_{T,\mu_B,\mu_Q} \geq \hspace{2cm} \\ 
        \hspace{2cm} \chi_2^B \left(\frac{\partial s}{\partial \mu_S}\bigg|_{T,\mu_B,\mu_Q}\right)^2+\chi_2^S \left(\frac{\partial s}{\partial \mu_B}\bigg|_{T,\mu_S,\mu_Q}\right)^2 \; , \\

.. math::
    \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}}\left[\chi_2^B\chi_2^Q-\left(\chi_{11}^{BQ}\right)^2\right] +2\chi_{11}^{BQ}\frac{\partial s}{\partial \mu_B}\bigg|_{T,\mu_S,\mu_Q} \frac{\partial s}{\partial \mu_Q}\bigg|_{T,\mu_B,\mu_S} \geq \hspace{2cm} \\
        \hspace{2cm} \chi_2^B \left(\frac{\partial s}{\partial \mu_Q}\bigg|_{T,\mu_B,\mu_S}\right)^2+\chi_2^Q \left(\frac{\partial s}{\partial \mu_B}\bigg|_{T,\mu_S,\mu_Q}\right)^2 \; , \\

.. math::
    \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}}\left[\chi_2^S\chi_2^Q-\left(\chi_{11}^{SQ}\right)^2\right] +2\chi_{11}^{SQ}\frac{\partial s}{\partial \mu_S}\bigg|_{T,\mu_B,\mu_Q} \frac{\partial s}{\partial \mu_Q}\bigg|_{T,\mu_B,\mu_S} \geq \hspace{2cm} \\
        \hspace{2cm} \chi_2^S \left(\frac{\partial s}{\partial \mu_Q}\bigg|_{T,\mu_B,\mu_S}\right)^2 +\chi_2^Q \left(\frac{\partial s}{\partial \mu_S}\bigg|_{T,\mu_B,\mu_Q}\right)^2 \; ,

and

.. math::
    \frac{\partial s}{\partial T}\bigg|_{\vec{\mu}}\left[\chi_2^B\chi_2^S\chi_2^Q+2\chi_{11}^{BS}\chi_{11}^{SQ}\chi_{11}^{BQ}-\sum_{j=B,S,Q}\chi_2^j \left(\chi_{11}^{j+1,j+2}\right)^2\right] \hspace{5cm} \\
        + \sum_{j=B,S,Q}\left[\left(\chi_{11}^{j,j+1}\right)^2-\chi_2^j\chi_2^{j+1}\right]\left(\frac{\partial s}{\partial \mu_{j+2}}\bigg|_{T,\mu_{\neq j+2}}\right)^2 \hspace{5cm} \\
        \hspace{1cm}+ 2 \sum_{j=B,S,Q}\left[ \chi_2^j\chi_{11}^{j+1,j+2}-\chi_{11}^{j,j+1}\chi_{11}^{j,j+2} \right]\frac{\partial s}{\partial \mu_{j+1}}\bigg|_{T,\mu_{\neq j+1}}\frac{\partial s}{\partial \mu_{j+2}}\bigg|_{T,\mu_{\neq j+2}} < 0 \; .


Stability and causality plots
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this sections are displayed the regions where the equation of state is either stable, acausal, unstable or both acausal and unstable. These regions are shown on 2D projections in terms of :math:`T` and :math:`\mu_i`.  
A line of constant :math:`\mu_i / T = 2.5`, which is the expected limit of application of the Taylor expansion employed here, is shown on each plot as an indication.

.. figure:: ../_images/Stability-T_vs_μB.png
   :alt: "T_muB"

.. figure:: ../_images/Stability-T_vs_μQ.png
   :alt: "T_muQ"

.. figure:: ../_images/Stability-T_vs_μS.png
   :alt: "T_muS"

.. figure:: ../_images/Stability-T_vs_μBQ.png
   :alt: "T_muBQ"

.. figure:: ../_images/Stability-T_vs_μB-Q.png
   :alt: "T_muB-Q"

.. figure:: ../_images/Stability-T_vs_μBS.png
   :alt: "T_muBS"

.. figure:: ../_images/Stability-T_vs_μB-S.png
   :alt: "T_muB-S"

.. figure:: ../_images/Stability-T_vs_μQS.png
   :alt: "T_muQS"

.. figure:: ../_images/Stability-T_vs_μQ-S.png
   :alt: "T_muQ-S"

.. figure:: ../_images/Stability-T_vs_μBQS.png
   :alt: "T_muBQS"

.. figure:: ../_images/Stability-T_vs_μ-BQS.png
   :alt: "T_mu-BQS"

.. figure:: ../_images/Stability-T_vs_μB-QS.png
   :alt: "T_muB-QS"

.. figure:: ../_images/Stability-T_vs_μBQ-S.png
   :alt: "T_muBQ-S"


References
~~~~~~~~~~

.. [1] N. Cruz-Camacho, R. Kumar, M. Reinke Pelicer, J. Peterson, T. A. Manning, R. Haas, V. Dexheimer, Veronica and J. Noronha-Hostler, `arXiv:2409.06837 <https://arxiv.org/abs/2409.06837>`__ [nucl-th]

