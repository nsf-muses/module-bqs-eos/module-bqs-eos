Parameters
----------
The 4D Taylor-expansion lattice (BQS) equation of state module has a variety of parameters for the calculation of the equation of state. The output file will correspond to the selected parameter choices.

As described in the `Quickstart <https://musesframework.io/docs/modules/eos_taylor_4d/_contents/quickstart.html>`__, the user must provide a `config.yaml`_ file. The purpose of this file is to supply all the configuration information required to run the module, such as input data and program options. The rules for this input file are specified in the `OpenAPI 3.0.0 Specifications file 
<https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/api/OpenAPI_Specifications-BQS_EoS.yaml?ref_type=heads>`__ provided with the module.

Each time the module is executed, it reads the `config.yaml`_ file provided
by the user and verifies that the input conforms to the OpenAPI
specifications. If it does not, module execution will be unsuccessful. When
this happens, make sure to check the logs to identify the issue with your
configuration.

It is not necessary to specify every single parameter in the configuration
file. If any parameter is omitted, it will be automatically assigned its 
default value. The default values for each parameter can be found in the 
OpenAPI specification. 

Upon successful execution, the module may produce several output files. These 
are described in the OpenAPI specification as part of the ``output`` schema. 
Not all of these files are created during every run, and some require the user 
to specify particular options in the `config.yaml`_ file.

Below are tables providing a brief overview of module's input, semi-internal and
output parameter files. These include parameter names, default values, and descriptions 
similar to those in the `OpenAPI specification file <https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/api/OpenAPI_Specifications-BQS_EoS.yaml?ref_type=heads>`__ for the module.

Input Parameters
~~~~~~~~~~~~~~~~

The input parameters required to execute the BQS module are presented in the table below. All parameters have default values in case the user does not specify any.
Complete details are given in the module's `OpenAPI specifications <https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/api/OpenAPI_Specifications-BQS_EoS.yaml?ref_type=heads>`__.

+--------------------------+-------------------------+----------------+----------------------------------------+
|**Category Description**  | **Input Parameter**     | **Default**    | **Description**                        |
+--------------------------+-------------------------+----------------+----------------------------------------+
| **Grid**                 | ``T_min``               | ``30.0``       |  Minimum temperature (in ``MeV``)      |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``T_max``               | ``600.0``      |  Maximum temperature (in ``MeV``)      |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``dT``                  | ``5.0``        |  Step size in temperature (in ``MeV``) |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_B_min``            | ``0.0``        |  Minimum baryon chemical potential     |
|                          |                         |                |  (in ``MeV``)                          |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_B_max``            | ``450.0``      |  Maximum baryon chemical potential     |
|                          |                         |                |  (in ``MeV``)                          |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``dmu_B``               | ``5.0``        |  Step size in baryon chemical          |
|                          |                         |                |  potential (in ``MeV``)                |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_Q_min``            | ``0.0``        |  Minimum electric charge chemical      |
|                          |                         |                |  potential (in ``MeV``)                |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_Q_max``            | ``450.0``      |  Maximum electric charge chemical      |
|                          |                         |                |  potential (in ``MeV``)                |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``dmu_Q``               | ``5.0``        |  Step size in electric charge chemical |
|                          |                         |                |  potential (in ``MeV``)                |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_S_min``            | ``0.0``        |  Minimum strange chemical potential    |
|                          |                         |                |  (in ``MeV``)                          |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``mu_S_max``            | ``450.0``      |  Maximum strange chemical potential    |
|                          |                         |                |  (in ``MeV``)                          |
|                          +-------------------------+----------------+----------------------------------------+
|                          | ``dmu_S``               | ``5.0``        |  Step size in strange chemical         |
|                          |                         |                |  potential (in ``MeV``)                |
+--------------------------+-------------------------+----------------+----------------------------------------+

Following is an example of a typical `config.yaml`_ for the BQS module:

 .. code:: console

  parameters:
    T_min: 30
    T_max: 50
    dT: 5
    mu_B_min: 0
    mu_B_max: 10
    dmu_B: 5
    mu_Q_min: 0
    mu_Q_max: 10
    dmu_Q: 5
    mu_S_min: 0
    mu_S_max: 10
    dmu_S: 5
  switchers:
    get_P: True 
    get_s_dens: True
    get_B_dens: True
    get_Q_dens: True
    get_S_dens: True
    get_E_dens: True
    get_c_s: True
    get_P_derivatives_finite_mu: True
    get_Chis_zero_mu: True
    get_dChisdT_zero_mu: True
    get_d2ChisdT2_zero_mu: True


Semi-internal variables
~~~~~~~~~~~~~~~~~~~~~~~

The input parameters required to run the BQS equation of state are the coefficients :math:`a_{0-9}`, :math:`b_{0-9}` and :math:`c_0` of the parameterization used to obtain the lattice-based susceptibilities, as explained in the `Physics overview <https://musesframework.io/docs/modules/eos_taylor_4d/_contents/physics.html>`__.

The default value of these parameters are obtained from reference [1]_. They are stored in `REF-BQS_eos_input_coef.yaml <https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/test/Ref_files/REF-BQS_eos_input_coef.yaml?ref_type=heads>`__, an auxiliary input file placed in the **test/Ref_files/** repository. 
In principle, one can update the values of the coefficients based on a similar fit to new lattice data. 
To do so, one can either simply change this same file as is, or place the resulting file, without the "REF-" prefix, in the **input/** repository, and execute the module by calling this input file after the `config.yaml`_ file.
Since this file is not required to run the module, the coefficients will be read from the "REF-" file by default.


Output Parameters
~~~~~~~~~~~~~~~~~

The output files generated by the module are in ``.csv`` format, and are stored in the **output/** directory, assuming they have to be produced according to the different switchers.

**BQS_eos_output_thermodynamics.csv**: this file contains values for thermodynamic observables such as pressure, energy density, charge densities, entropy density or second-order derivatives of
pressure, as a function of temperature :math:`T` and the conserved charge chemical potentials, *i.e.* :math:`\mu_B`, :math:`\mu_Q` and :math:`\mu_S`.

**BQS_eos_output_chis.csv**: this file contains the values of all susceptibilities of :math:`0^{th}`, :math:`2^{nd}` and  :math:`4^{th}` order used in the construction of the EoS, as a function of the chosen temperature grid, at zero chemical potentials only.


**References**


.. [1] S. Borsanyi, Z. Fodor, J. N. Guenther, S. K. Katz, K. K. Szabo, A. Pasztor, I. Portillo, and C. Ratti, *JHEP 10, 205* (2018), `arXiv:1805.04445 <https://arxiv.org/abs/1805.04445>`__ [hep-lat]


.. _config.yaml: https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/input/config.yaml

