Quickstart
----------

In this section, you will find information on how to quickly run this 
module after a few simple steps.

In order to use the most up-to-date function version of the Isint-2DTExS 
EoS module, replace ``<TAG>`` in the following instructions by the version
indicated on the |CE-registry|_ webpage.

On the Calculation Engine (CE)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to run the BQS EoS module on the MUSES |CE|_, refer to the 
`user manual's Quick Start Guide <https://musesframework.io/docs/user/quickstart.html>`__ to find instructions and exemples on how to submit jobs and retrieve the run outputs.

BQS EoS module's characteristics to know when running on CE:

 - **Name:** ``'eos_taylor_4d'`` is the name of the module in the |CE-registry|_

Locally
~~~~~~~

Using Docker
^^^^^^^^^^^^

The quickest way to run the BQS EoS module on your machine is by using
the Docker container. First, make sure that you have
`Docker <https://www.docker.com/>`__ locally installed on your machine.

1. Pull Docker image
''''''''''''''''''''

The recommended way to obtain the BQS EoS Docker container is to
pull the latest version from the GitLab Container Registry::

   docker pull registry.gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos:{TAG}

2. Run the module from the container
''''''''''''''''''''''''''''''''''''

In order to run this container, you must at least have a minimal project
directory, with an **input/** folder containing a |config|_ file that is
used to define the module's parameters for the run, and and **output/** folder.
More information about the content of the |config|_ file can be found in
the module's |specs|_ and can be changed based on the user's requirements.. A code block for how the config.yaml file is also provided below.

.. code-block:: yaml

   parameters:
     T_min: 30
     T_max: 50
     dT: 5
     mu_B_min: 0
     mu_B_max: 10
     dmu_B: 5
     mu_Q_min: 0
     mu_Q_max: 10
     dmu_Q: 5
     mu_S_min: 0
     mu_S_max: 10
     dmu_S: 5
   switchers:
     get_P: True 
     get_s_dens: True
     get_B_dens: True
     get_Q_dens: True
     get_S_dens: True
     get_E_dens: True
     get_c_s: True
     get_P_derivatives_finite_mu: True
     get_Chis_zero_mu: True
     get_dChisdT_zero_mu: True
     get_d2ChisdT2_zero_mu: True

Once you have defined the parameters of your choice, you can run the container::
   
   export DOCKER_IMAGE_NAME=registry.gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos
   export DOCKER_IMAGE_TAG=<TAG>
   docker run -it --rm --name BQS \
      -v "${PWD}/input:/opt/input" \
      -v "${PWD}/output:/opt/output" \
      $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG python src/main.py input/config.yaml

The output files will appear in the **output/** directory upon completion.

Without Docker
^^^^^^^^^^^^^^

In order to run the module without Docker, one has to copy the |repo|_ on their 
local machine, and install the required libraries listed in |requir|_.

Start by downloading the |repo|_ on your machine, either by going to the webpage
and downloading it, or by using the command line::

   git clone --depth 1 --branch <TAG> https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos.git

1. Install required packages
''''''''''''''''''''''''''''

In order to run the module locally, the local machine must have an up-to-date
version of the `GCC compiler <https://gcc.gnu.org/>`__ to compile and build the
core code of the module written in C.
 
Running the module locally also requires an up-to-date version of
`python3 <https://www.python.org/>`__ installed, including the following packages
(listed in |requir|_):

- `numpy <https://numpy.org/>`__
- `pyyaml <https://pyyaml.org/>`__

- `openapi-core <https://github.com/python-openapi/openapi-core>`__
- `muses_porter <https://gitlab.com/nsf-muses/common/-/tree/main/Porter>`__

  **Note:** we do recommend the use of a Python virtual environment to avoid 
  potential conflicts with your local package installation. To create and 
  activate a virtual environment, execute the following commands::

     python3 -m venv .venv
     source .venv/bin/activate

  Whenever you need to deactivate the virtual environment, simply type ``deactivate``.

These libraries can all be easily installed using the ``pip`` package manager.
Moreover, one can proceed by calling directly |requir|_, using::

   pip install --user -r module-bqs-eos/requirements.txt

If the installation of the Porter fails by this method, see more detailed instructions 
in the Porter `README <https://gitlab.com/nsf-muses/common/-/tree/main/Porter/README.md>`__.
 

2. Compile and build
''''''''''''''''''''

After successful installation of the required libraries, you need to compile
and build the core code of the module::

   cd module-bqs-eos/
   make

The executable **EoS_BQS** will be created upon successful compilation and 
linking.


3. Run the module
'''''''''''''''''

Once all of the dependencies are successfully installed, and the project
has been compiled, all that's left to do is run the module.

First make sure that you have created a
`config.yaml
<https://gitlab.com/nsf-muses/chiral-eft-eos/chiral_eft_eos/-/blob/main/input/config.yaml>`__
containing the module configuration in the **input/** directory. Then run::

   python src/main.py input/config.yaml

The output files will appear in the **output/** directory upon completion.


..
  -------------------
  | Fixed variables |
  -------------------

.. |repo| replace:: BQS EoS repository
.. _repo: https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/tree/main?ref_type=heads

.. |requir| replace:: **requirements.txt**
.. _requir: https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/requirements.txt?ref_type=heads

.. |config| replace:: **config.yaml**
.. _config: https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/input/config.yaml?ref_type=heads

.. |specs| replace:: OpenAPI specifications
.. _specs: https://gitlab.com/nsf-muses/module-bqs-eos/module-bqs-eos/-/blob/main/api/OpenAPI_Specifications-BQS_EoS.yaml?ref_type=heads

.. |CE| replace:: CE
.. _CE: https://alpha.muses.ncsa.illinois.edu/

.. |CE-registry| replace:: CE modules registry
.. _CE-registry: https://alpha.muses.ncsa.illinois.edu/ce/modules/
