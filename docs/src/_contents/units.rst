Units
-----

The BQS code uses megaelectronvolt (:math:`MeV`) as the preferred unit.
The table below contain the units used for all output quantities (if not specified, the quantity is dimensionless and has thus no unit).

+----------------------------------------------------+-----------------+
| **Variable**                                       | **Unit**        |
+====================================================+=================+
| Temperature (:math:`T`)                            | MeV             |
+----------------------------------------------------+-----------------+
| Baryon chemical potential (:math:`\mu_B`)          | MeV             |
+----------------------------------------------------+-----------------+
| Electric charge chemical potential (:math:`\mu_Q`) | MeV             |
+----------------------------------------------------+-----------------+
| Strange chemical potential (:math:`\mu_S`)         | MeV             |
+----------------------------------------------------+-----------------+
| Pressure (:math:`P`)                               | MeV :math:`^4`  |  
+----------------------------------------------------+-----------------+
| Baryon density (:math:`n_B`)                       | MeV :math:`^3`  |   
+----------------------------------------------------+-----------------+
| Electric charge density (:math:`n_Q`)              | MeV :math:`^3`  |   
+----------------------------------------------------+-----------------+
| Strangeness density (:math:`n_S`)                  | MeV :math:`^3`  |   
+----------------------------------------------------+-----------------+
| Entropy density (:math:`s`)                        | MeV :math:`^3`  |
+----------------------------------------------------+-----------------+
| Energy density (:math:`\varepsilon`)               | MeV :math:`^3`  |
+----------------------------------------------------+-----------------+
| Speed of sound (:math:`{c_s}^2`)                   |  *No unit*      |
+----------------------------------------------------+-----------------+
| Second-order derivatives of pressure               | MeV :math:`^2`  |
+----------------------------------------------------+-----------------+

